import numpy as np
import matplotlib.pyplot as plt

def Xt(a, b, t):
    return (a + b)*np.cos(t) + b*np.cos((a + b)/b * t) 

def dXt(a, b, t):
    return -(a + b)*np.sin(t) - (a + b) * np.sin((a + b)/b * t)


def Yt(a, b, t):
    return (a + b)*np.sin(t) + b*np.sin((a + b)/b * t)

def dYt(a, b, t):
    return (a + b)*np.cos(t) + (a + b) * np.cos((a + b)/b * t)


def out_of_center(a, b, t, radius = 2):
    """
    Preveri ali se vrednost na hypotrochoidi ob času t nahaja izven kroga s središčem v (0, 0) z radijem radius.
    """
    x = Xt(a, b, t)
    y = Yt(a, b, t)
    return x**2 + y**2 > radius**2
    
def in_box(a, b, t_old, t, box_width = 0.2):
    """
    Preveri ali se vrednost na hypotrochoidi ob času t nahaja v škatli širine box_width
    okoli vrednosti hypotrochoide ob času t_old.
    """
    x_old = Xt(a, b, t_old)
    y_old = Yt(a, b, t_old)
    
    x_below = x_old - box_width/2
    x_above = x_old + box_width/2
    
    y_below = y_old - box_width/2
    y_above = y_old + box_width/2
    
    x = Xt(a, b, t)
    y = Yt(a, b, t)
    return x_below <= x <= x_above and y_below <= y <= y_above
    
def remove_duplicates(t_pairs, round_at_dec = 2):
    """
    Iz seznama, ki je sestavljen iz podseznamov velikosti 2, odstrani vse ponovitve podseznamov.
    Da lahko preveri ali sta dva podseznama enaka, vrednosti v podseznamih v času preverjanja
    zaokroži na round_at_dec decimalnih mest.
    """
    returns = []
    for t_pair in t_pairs:
        t_pair_round = np.round(t_pair, round_at_dec)
        already_in = False
        
        for t_pair_return in returns:
            t_pair_return_round = np.round(t_pair_return, round_at_dec)
            if (t_pair_round == t_pair_return_round).all():
                already_in = True
                break
            
        if not already_in:
            returns.append(t_pair)
                
    returns = np.array(returns)
    return returns
     
def find_min_and_max_t(t_pairs):
    """
    V seznamu, ki je sestavljen iz podseznamov velikosti 2, najde vrednost najmnajšega in največjega elementa.
    """
    flatten_ts = [item for sublist in t_pairs for item in sublist]
    return min(flatten_ts), max(flatten_ts)

def find_aproximative_t_pairs_for_intersections(a, b, t_distance = 5, box_width = 0.2, radius = 2, t_step = 0.3):
    """
    Pregleduje vrednosti hypotrochoide ob časih med t = 0 in t = 70 (z gledanjem animacije, ki jo izriše funkcija 
    animation_hypotrochoida(a, b, ts) na oko ocenjen čas enega obhoda hypotrochoide), s korakom t_step. 
    Če je vrednost hypotrochoide ob obiskanem času izven kroga z radijem radius (ker iščem le presečišča na zunanjem robu hypotrochoide)
    in če vrednost hypotrochoide ob tem času pade v box širine box_width okrog katere od že obiskanih točk (tiste v t_visited),
    pri čemer sta časa vsaj t_distance narazen (ker me zanimata časa, ko krivulja prečka dano presečišče v eni in v drugi smeri),
    potem sta ta dva časa dober začetni približek za iskanje presečišča ob katerem se nahajata. Ta dva časa shranim v seznam ts_aproximative.
    Če vrednost hypotrochoide ob obiskanem času ne pade v box od katere od že obiskanih točk, vendar je vrednost ob tem času izven 
    kroga z radijem radius, čas shranim v seznam t_visited.
    Vrnjen seznam ts_aproximative je oblike
    [[t1_aprox_for_p1, t2_aprox_for_p1], [t1_aprox_for_p2, t2_aprox_for_p2], ..., [t1_aprox_for_p7, t2_aprox_for_p7]],
    kjer so presečišča oštevilčena poljubno (ni vrstnega reda med presečišči).
    """
    ts = np.arange(0, 70, t_step)
    ts_aproximative = []
    ts_visited = []
    
    for t_idx in range(0, len(ts)):
        used = False
        used_idx = 0
        for t_old_idx in range(0, len(ts_visited)):
            if abs(ts_visited[t_old_idx] - ts[t_idx]) >= t_distance:
                
                if in_box(a, b, ts_visited[t_old_idx], ts[t_idx], box_width) and out_of_center(a, b, ts[t_idx], radius):
                    ts_aproximative.append([ts_visited[t_old_idx], ts[t_idx]])
            
                    used = True
                    used_idx = t_old_idx
                    break
        if used:
            del ts_visited[used_idx]
        else:
            if out_of_center(a, b, ts[t_idx], radius):
                ts_visited.append(ts[t_idx])
        
    return ts_aproximative


def find_intersections_with_newton_sistem(a, b, aproximative_t_pairs_for_intersections, epsilon):
    """
    Z uporabo newtonove metode za reševanje sistema enačb z uporabo začetnih približkov za presečišča, ki so shranjena v seznamu
    aproximative_t_pairs_for_intersections, ki je oblike: 
    [[t1_aprox_for_p1, t2_aprox_for_p1], [t1_aprox_for_p2, t2_aprox_for_p2], ..., [t1_aprox_for_p7, t2_aprox_for_p7]], 
    kjer so presečišča oštevilčena poljubno (ni vrstnega reda med presečišči),
    za vsako presečišče izračuna prvi in drugi čas, ko ga krivulja prečka, na absolutno natančnost epsilon.
    """    
    f1 = lambda t1, t2: Xt(a, b, t1) - Xt(a, b, t2)
    f2 = lambda t1, t2: Yt(a, b, t1) - Yt(a, b, t2)
    
    F = np.array([f1, f2])
    
    df1t1 = lambda t1, t2: dXt(a, b, t1)
    df1t2 = lambda t1, t2: -dXt(a, b, t2)
    
    df2t1 = lambda t1, t2: dYt(a, b, t1)
    df2t2 = lambda t1, t2: -dYt(a, b, t2)
    
    J = np.array([[df1t1, df1t2],
                  [df2t1, df2t2]])
    
    epsilon = 10**(-10)
    t_pairs_intersections = []

    for t_pair in aproximative_t_pairs_for_intersections:
        t0 = np.array(t_pair) #začetna približka
        t = newton_method_sistem(F, t0, epsilon, J, max_num_of_it = 50)          
        t_pairs_intersections.append(t)
        
    return t_pairs_intersections

def cross_product(p1, p2):
    """
    Izračuna vektorski produkt med 2D vektorjema p1 in p2.
    """
    return p1[0] * p2[1] - p1[1] * p2[0]

def is_before(p1, p2, round_at_dec = 2): #uboje je tuple
    """
    Vrne True, če je točka p1 pred točko p2. Vrstni red je določen s smerjo urinega kazalca z začetkom 
    na negativni strani x osi.
    Funkcija deluje ob predpostavki, da na istem delu x osi (pozitivni oziroma negativni del) nimamo dveh točk 
    s koordinato y = 0, kar za to nalogo drži.
    """
    p1x = round(p1[0], round_at_dec)
    p1y = round(p1[1], round_at_dec)
    #p2x = round(p2[0], round_at_dec) # ne rabim
    p2y = round(p2[1], round_at_dec)
    

    if p1x < 0 and p1y == 0: 
        return True #sicer take točke nimamo vendar taka točka bi vedno imela prednost
    
    if p1y > 0 and p2y < 0: #ker grem v smeri ure z začetnom od negativne strani x osi imajo točke nad y osjo prednost pred tistimi pod
        return True
    
    elif p1y < 0 and p2y > 0:
        return False
    
    if p1y* p2y >= 0: # če sta obe točki nad ali obe pod y osjo (lahko je ena od njiju tudi na x osi)
        return cross_product(p1, p2) < 0 # če to drži je p1 pred p2
    
            
    
def sort_times(a, b, t_pairs):
    """
    Sprejme array t_pairs, ki vsebuje 7 podseznamov dolžine 2 in je oblike 
    [[p1_first_time, p1_second_time], [p2_first_time, p2_second_time], ..., [p7_first_time, p7_second_time]],
    kjer so presečišča oštevilčena poljubno (ni vrstnega reda med presečišči).
    Vsak podseznam vsebuje prvi in drugi čas, ko krivulja prečka i-to presečišče.
    Vrne seznam enake oblike, pri čemer presečišča z algoritmom urejanja z vstavljanjem uredi v smeri 
    urinega kazalca z začetkom na negativni strani x osi.
    """
    #oba časa v istem podseznamu opisujeta isto točko zato lahko delam le s prvim elementom iz vsakega podseznama
    for i in range(0, len(t_pairs) - 1):
        j = i
        t_current = t_pairs[j][0]
        x_current = Xt(a, b, t_current)
        y_current = Yt(a, b, t_current)
        
        t_next = t_pairs[j + 1][0]
        x_next = Xt(a, b, t_next)
        y_next = Yt(a, b, t_next)
        
        while j >=0 and is_before((x_next, y_next), (x_current, y_current)):
            t_pairs[[j, j+1]] = t_pairs[[j + 1, j]]
            #t_pairs[j], t_pairs[j + 1] = t_pairs[j + 1], t_pairs[j] # tole ni ok, ker mam np.array
            j = j - 1
            
            t_current = t_pairs[j][0]
            x_current = Xt(a, b, t_current)
            y_current = Yt(a, b, t_current)
        
            t_next = t_pairs[j + 1][0]
            x_next = Xt(a, b, t_next)
            y_next = Yt(a, b, t_next)
            
    return t_pairs  
    
    
def find_t_pairs_of_boundary_curve(a, b, t_pairs):
    """
    Sprejme array t_pairs, ki vsebuje 7 podseznamov dolžine 2 in je oblike 
    [[p1_first_time, p1_second_time], [p2_first_time, p2_second_time], ..., [p7_first_time, p7_second_time]].
    Vsak podseznam vsebuje prvi in drugi čas, ko krivulja prečka i-to presečišče.
    Vrne array t_pairs_of_boundary_curve, ki vsebuje 7 podseznamov dolžine 2 in je oblike 
    [[t_p1_out, t_p2_in], [t_p2_out, t_p3_in], ..., [t_p7_out, t_p1_in]].
    Oznaka in pomeni, da krivulja v smeri urinega kazalca pripotuje v presečišče pi​, oznaka out pa, da iz njega v smeri 
    urinega kazalca potuje naprej. Presečišča si sledijo v smeri urinega kazalca z začetkom na negativni strani x osi.
    Če se hypotrochoido izriše od t_out do t_in na vsakem od teh intervalov, se izriše zunanji rob hypotrochoide.
    """ 
    #print(t_pairs)
    t_min, t_max = find_min_and_max_t(t_pairs)
    #print("min_t max_t:", t_min, t_max)
    
    t_pairs_of_boundary_curve = []
    for i in range(0, len(t_pairs) - 1):
        dist_x = abs(t_pairs[i][0] - t_pairs[i+1][0])
        dist_y = abs(t_pairs[i][1] - t_pairs[i+1][1])
        dist_xy = abs(t_pairs[i][0] - t_pairs[i+1][1])
        dist_yx = abs(t_pairs[i][1] - t_pairs[i+1][0])
        
        distances = [dist_x, dist_y, dist_xy, dist_yx]
        
        #ker iščem zunanjo mejo hypotrochoide, moram izmed zaporednih parov časov, 
        #najti tista dva (enaga, ki pripada presečišču i in enega, ki pripada presečišču i + 1)
        #med katerima preteče najmanj časa (kar pomeni, da gre hypotrochoida direktno iz točke i v točko i + 1)
        idx = distances.index(min(distances)) 
        
        #točki med katerima je razdalja najmanjša shranim v seznam
        if idx == 0:
            t_pairs_of_boundary_curve.append([t_pairs[i][0], t_pairs[i+1][0]])
        elif idx == 1:
            t_pairs_of_boundary_curve.append([t_pairs[i][1], t_pairs[i+1][1]])
        elif idx == 2:
            t_pairs_of_boundary_curve.append([t_pairs[i][0], t_pairs[i+1][1]])
        elif idx == 3:
            t_pairs_of_boundary_curve.append([t_pairs[i][1], t_pairs[i+1][0]])
            
    #t_start = 0 #start krivulje
    
    #da bo zunanji rob hypotrochoide sklenjen, je v seznam je treba dodati še časa za potek krivulje od zadnjega presečišča p7 do začetnega p1
    
    #čas ko se krivulja sklene t_end, lahko poiščem na dva načina
    #(1) z gradientnim spustom z začetnim približkom t_max
    epsilon = 10**(-10)
    step = 0.001
    f = lambda t: Xt(a, b, t)
    df = lambda t: dXt(a, b, t)
    
    start_t = t_max
    t_end = gradient_descent(f, start_t, step, epsilon, df, method = "minimum")
    #print("t_end gradient descent:", t_end)
    
    #(2)
    #iz časovnega poteka krivulje (če začnem ob t = 0) vem, da krivulja obišče presečišče p7 prvo (to je ob času t_min)
    #presečišče p1, ki je nad njim, pa najkasneje (ob t_max)
    #ts = np.arange(0, 70.01, 0.1)
    #plot_hypotrochoida(a, b, ts)
    #plot_point(a, b, t_min, color = "blue") #presečišče p7
    #plot_point(a, b, t_max, color = "red") # presečišče p1
    
    #iz p1 se potem spet vrača v p7 (če ob krivulji sledimo ob naraščanju t)
    #zato velja, da če t_max prištejem t_min dobim t_end
    #t_end = t_max + t_min
    #print("t_end calculation:", t_end)
    
    #sedaj lahko čas, ko krivulja štarta iz spodnjega presečišča p7 v zgornjega p1, dobim kot t_end + t_min
    t_pairs_of_boundary_curve.append([t_end + t_min, t_max]) #začetni in končni čas iz presečišča 7 v 1
    #print("čas konca krivulje:", t_end)
    
    return t_pairs_of_boundary_curve
            
            
        
        
    

def area_hypotrochoida(a, b, epsilon, direction = "x", animated = False):
    """
    Izračuna ploščino hypotrochoide pri parametrih a in b na absolutno natančnost epsilon.
    Najprej funkcija najde dobre približke za presečišča na zunanjem robu hypotrochoide. Te približke nato uporabi 
    za izračun točnih časov prvega in drugega obhoda točk, ko hypotrochoida seka samo sebe. S pomočjo izračunaih presečišč 
    izračuna ploščino zunanjega roba hypotrochoide.
    """
    #najdem dobre približke za presešiča na robnem delu hypotrochoide
    aproximative_t_pairs_for_intersections = find_aproximative_t_pairs_for_intersections(a, b)
    
    #z najdenimi približki, z uporabo newtonove metode za reševanje sistema enačb, izračunam presečišča, ki pa vsebujejo tudi dvojnike
    #(saj več parov približkov presečišč skonvergira k istemu presečišču)
    t_pairs_intersections = find_intersections_with_newton_sistem(a, b, aproximative_t_pairs_for_intersections, epsilon)
    #print(len(t_pairs_intersections)) #oba časa v istem podseznamu opisujeta isto točko
    
    #izbrišem dvojnike
    t_pairs_intersections = remove_duplicates(t_pairs_intersections)
    #print(len(t_pairs_intersections))
    
    #presečišča sortiram tako, da so razvrščena v smeri urinega kazalca z začetkom pri negativnem delu x osi
    t_pairs_intersections = sort_times(a, b, t_pairs_intersections)
    
    #iz izračunanih presečišč (ker vsako presečišče trenutno definirata dva časa (oba časa ob katerih krivulja seka samo sebe na določeni točki)
    #izluščim tiste, s pomočjo katerih se lahko sprehodim po zunanjem robu hypotrochoide v smeri urinega kazalca
    #(izmed temi potem izbiram po katerih intervalih integriram)
    #(če se nariše potek hypotrochoide na graf med začetnim in končnim časom iz vsakega podseznama potem se izriše celotno zun območje hypotrochoide)
    t_pairs_of_boundary_curve = find_t_pairs_of_boundary_curve(a, b, t_pairs_intersections)
    #print("seznam časov izvorov in ponorov v presečišča:", t_pairs_of_boundary_curve)

    #narišem zunanji rob hypotrochoide
    #plot_boundary_curve(a, b, t_pairs_of_boundary_curve)
    
    if animated and direction == "x":
        plt.clf()
        ts = np.arange(0, 70.01, 0.1)
        plot_hypotrochoida(a, b, ts)
        #narišem sortirana presečišča
        plot_intersections(a, b, t_pairs_intersections)
  
    if direction == "x":
        # integriram spodnjo funkcijo
        fun_x = lambda t: Yt(a, b, t) * dXt(a, b, t)
        
        # integriranje območja med točko na neg delu x osi (point0) in point1 
        t_p7_out = t_pairs_of_boundary_curve[-1][0] # večji čas, to je t_max + 2*t_min
        t_p1_in = t_pairs_of_boundary_curve[-1][1] # manjši čas, to je t_max
        #ker t_p7_out > t_p1_in, čas ko krivulja drugič obišče točko (point0), ki jo je obiskala ob t = 0, lahko dobim kot:
        #t_p0 = t_p7_out  - abs(t_p7_out -  t_p1_in)/2
        #print("t_p0 calculated:", t_p0) #t_p0 je v tem primeru čas, ko se krivulja zaključi
        
        #ali pa t_p0 dobim z gradientnim spustom z začetnim približkom t_p7_out ali t_p1_in
        step = 0.001
        f = lambda t: Xt(a, b, t)
        df = lambda t: dXt(a, b, t)
        
        start_t = t_p7_out 
        t_p0 = gradient_descent(f, start_t, step, epsilon, df, method = "minimum")
        #print("t_p0 gradient descent:", t_p0)
        
        #start_t = t_p1_in
        #t_p0 = gradient_descent(f, start_t, step, epsilon, df, method = "minimum")
        #print("t_p0 gradient descent:", t_p0)
        
        #vrednost integrala med point0 in point1 pointegrirano v smeri x
        if animated:
            t_pairs = [[t_p0, t_p1_in]]
            plot_boundary_curve(a, b, t_pairs)
            fill_area(a, b, t_pairs, below = 0, above = Yt)
        I_x01, n = romberg(fun_x, t_p0, t_p1_in, epsilon) 
        #print("I_x01:", I_x01)
        
        # integriranje območja med točkama p1 in p2
        t_p1_out = t_pairs_of_boundary_curve[0][0]
        t_p2_in = t_pairs_of_boundary_curve[0][1]
        if animated:
            t_pairs = [[t_p1_out, t_p2_in]]
            plot_boundary_curve(a, b, t_pairs)
            fill_area(a, b, t_pairs, below = 0, above = Yt)
        I_x12, n = romberg(fun_x, t_p1_out, t_p2_in, epsilon)
        #print("I_x12:", I_x12)
        
        # integriranje območja med točkama p2 in p3
        t_p2_out = t_pairs_of_boundary_curve[1][0]
        t_p3_in = t_pairs_of_boundary_curve[1][1]
        if animated:
            t_pairs = [[t_p2_out, t_p3_in]]
            plot_boundary_curve(a, b, t_pairs)
            fill_area(a, b, t_pairs, below = 0, above = Yt)
        I_x23, n = romberg(fun_x, t_p2_out, t_p3_in, epsilon)
        #print("I_x23:", I_x23)
        
        # integriranje območja med točkama p3 in p4
        t_p3_out = t_pairs_of_boundary_curve[2][0]
        t_p4_in = t_pairs_of_boundary_curve[2][1]
        if animated:
            t_pairs = [[t_p3_out, t_p4_in]]
            plot_boundary_curve(a, b, t_pairs)
            fill_area(a, b, t_pairs, below = 0, above = Yt)
        I_x34, n = romberg(fun_x, t_p3_out, t_p4_in, epsilon)
        #print("I_x34:", I_x34)
        
        #ploščina zgornjega dela hypotrochoide (nad x osjo) 
        I_x_upper = I_x01 + I_x12 + I_x23 + I_x34
        #print("I x upper:", I_x_upper)
        
        # integriranje spodnjega dela (ki je pravzaprav enak kot zgornji in bi lahko le zgornji rezultat množila z 2)
        # integriranje območja med točkama p0 in p7
        if animated:
            t_pairs = [[t_p0, t_p7_out]]
            plot_boundary_curve(a, b, t_pairs)
            fill_area(a, b, t_pairs, below = Yt, above = 0)
        I_x07, n = romberg(fun_x, t_p0, t_p7_out, epsilon)
        #print("I_x07:", I_x07)
        
        # integriranje območja med točkama p7 in p6
        t_p6_put = t_pairs_of_boundary_curve[5][0]
        t_p7_in = t_pairs_of_boundary_curve[5][1]
        if animated:
            t_pairs = [[t_p7_in, t_p6_put]]
            plot_boundary_curve(a, b, t_pairs)
            fill_area(a, b, t_pairs, below = Yt, above = 0)
        I_x76, n = romberg(fun_x, t_p7_in, t_p6_put, epsilon)
        #print("I_x76:", I_x76)
        
        # integriranje območja med točkama p6 in p5
        t_p5_out = t_pairs_of_boundary_curve[4][0]
        t_p6_in = t_pairs_of_boundary_curve[4][1]
        if animated:
            t_pairs = [[t_p6_in, t_p5_out]]
            plot_boundary_curve(a, b, t_pairs)
            fill_area(a, b, t_pairs, below = Yt, above = 0)
        I_x65, n = romberg(fun_x, t_p6_in, t_p5_out, epsilon)
        #print("I_x65:", I_x65)
        
        # integriranje območja med točkama p5 in p4
        t_p4_out = t_pairs_of_boundary_curve[3][0]
        t_p5_in = t_pairs_of_boundary_curve[3][1]
        if animated:
            t_pairs = [[t_p5_in, t_p4_out]]
            plot_boundary_curve(a, b, t_pairs)
            fill_area(a, b, t_pairs, below = Yt, above = 0)
        I_x54, n = romberg(fun_x, t_p5_in, t_p4_out, epsilon)
        #print("I_x54:", I_x54)
        
        I_x_bottom = - (I_x07 + I_x76 + I_x65 + I_x54)
        #print("I_x_bottom:", I_x_bottom)
        
        I = I_x_upper + I_x_bottom
        print("I", I) #rezultat 14.15819752098852
        
    if direction == "y":
        pass
        #najdem minimum po y (t_min) tko da uporabim gradient_descent z zač približkom t_p6_in ali z t_p5_out
        #in zgornji t_max z zač približkom t_p2_out ali t_p3_in
        
        #levi del
        # inegriram fun_y = lambda t: Xt(a, b, t) * dYt(a, b, t) od t_min do t_p6_in in od t_p6_out do t_p7_in in od t_p7_out do t_p0
        # te integrali bojo z minusom, ker sem na neg delu x osi in in integriram v pravi smeri y
        # potem integriram fun_y od t_p0 do t_p1_in in od t_p1_out do t_p2_in in od t_p2_out do t_max, kar bo tudi z minus 
        # oziroma ta integral je enak prejšnjemnu zaradi simetričnosti in bi loh kr samo tega dvakrat
        
        
        #desni del 
        # dobim tako da grem od t_p4_in do t_p3_out in od t_p3_in do t_max (to bo s +, ker sem na + delu x osi in integriram v pravi smeri y)
        # in to pomnožim z 2
        
    if direction == "with circle":
        #drugi način računanja ploščine bi lahko bil izračun ploščine kroga, z radijem ki seže do zunanjih presečišč
        # tej plščini bi se prištela 7 * pliščina enega cveta "rože", ki štrli iz kroga
        
        #integriranje ploščine kroga
        def calculate_radius(a, b, t_pairs_of_boundary_curve):
            """
            Iz seznama oblike [[t_p1_out, t_p2_in], [t_p2_out, t_p3_in], ..., [t_p7_out, t_p1_in]]., kjer
            vsak podseznam vsebuje začetni in končni čas poteka krivulje od presečišča i do presečišča i + 1,
            izračuna radije kroga, ki se dotikajo vsakega od teh presečišč. 
            """
            radius = []
            for t_pair in t_pairs_of_boundary_curve:
                t = t_pair[0]
                x = Xt(a, b, t)
                y = Yt(a, b, t)
                radius.append(np.sqrt(x**2 + y**2))
            return radius
        
        def check_if_circle(radius, epsilon):
            """
            Funkcija preveri ali so vsi elementi v seznamu radius enaki na absolutno natančnost epsilon.
            """
            round_at_dec = int(abs(np.log10(epsilon)))
            #print(round_at_dec)
            r = round(radius[0], round_at_dec)
            
            for r in radius[1:]:
                if round(r, round_at_dec) != r:
                    return False
            return True
                    
        #iskanje radia kroga, ki se dotika zunanjih presečišč krivulje
        # (če bi računala ploščino le s to metodo bi se lahko izgonila računanju vseh presečišč in bi lahko na oko z gledanjem animacije
        # ocenila začetna časa za eno izmed presečišč in na njiju pognala newtonovo metodo za reševanje sistemov)
        radius = calculate_radius(a, b, t_pairs_of_boundary_curve)
        #print(radius[0])
        #print("radius:", radius)
        #print(check_if_circle(radius, epsilon)) 
        #vrne False, zadnji radij je izračunan le na 8 enakih decimalk kor prvi
        #tudi če še povečam natančnost pri funkciji za iskanje presečišč so radiji enaki
        
        fun_circle = lambda x: np.sqrt(radius[0]**2 - x**2)
        #iskanje časa ko krivulja naredi prvi obhod (čas levega extrema) z gradientnim spustom (kot prej)
        step = 0.001
        f = lambda t: Xt(a, b, t)
        df = lambda t: dXt(a, b, t)
        
        t_p7_out = t_pairs_of_boundary_curve[-1][0] 
        start_t = t_p7_out 
        t_p0 = gradient_descent(f, start_t, step, epsilon, df, method = "minimum")
        
        #računanje ploščine kroga (2*ploščina zgornjega dela kroga)
        x1 = -radius[0]
        t_p4_in = t_pairs_of_boundary_curve[2][1]
        x2 = radius[0]
        I_x04_circle, n = romberg(fun_circle, x1, x2, epsilon)
        I_circle = 2*I_x04_circle
        
        #račuanje ploščine cvetnega lista (izberem cvetni list med presečiščema p1 in p2)
        # integriranje območja med točkama p1 in p2
        fun_x = lambda t: Yt(a, b, t) * dXt(a, b, t)
        t_p1_out = t_pairs_of_boundary_curve[0][0]
        t_p2_in = t_pairs_of_boundary_curve[0][1]
        I_x12_fun, n = romberg(fun_x, t_p1_out, t_p2_in, epsilon)
        #print("I_x12_fun:", I_x12_fun)
        
        # integriranje kroga med točkama p1 in p2 (ki ga odštejem od zgornjega integrala, da dabim le ploščino lista)
        x1 = Xt(a, b, t_p1_out)
        x2 = Xt(a, b, t_p2_in)   
        I_x12_circle, n = romberg(fun_circle, x1, x2, epsilon)
        #print("I_x12_circle:", I_x12_circle)
        
        I_flower_part = I_x12_fun - I_x12_circle #ploščina cvetnega lista
        #print("I_flower_part:", I_flower_part)
        
        #celotna ploščina krivulje
        I = I_circle + 7*I_flower_part
        print("I:", I) #rezultat 14.158197590004214 #rezultat ima le 7 enakih decimalk kot tisti po prejšnji metodi
        #rezultat 14.1581975905538 izračunan z natančnostjo 10**(-12)
        
        # Do neskladanj z rezultati pride verjetno zaradi dodatnih seštevanj različnih delov ploščin, kar prinese dodatne napake
        # Že pri 1. metodi (integriranje hypotrochoida po dx) se posamezne ploščine (ki so izračunane na natančnost epsilon) med seboj seštevajo
        # in tako končen rezultat lahko da ni več dovolj točen.
        
        # Pri nalogi računanja integrala porazdelitvene funkcije verjetnosti sem integralu prištevala vrednost np.sqrt(np.pi)/2
        # vendar sem to vrednost prištevala znotraj metode, ki računa integral in je bil zato končen rezultat kljub temu dovolj točen.
        # Tu pri tej nalogi pa ne vem kako obržati dovolj dobro natačnost končnega rezultata. Pomagalo bi verjetno
        # če bi epsilon še povečala recimo iz 10**(-10), kar želim imeti na koncu, na recimo 10**(-13)?
        
         
#    if animated:
#        plt.savefig("images/hypotrochoida_area")
        
    return I
    
    
# *********************** U P O R A B L J E N I    A L G O R I T M I    N U M E R I Č N E     M A T E M A T I K E *******************

def obratno_vstavljanje(U, b):
    """
    Reši sistem enačb Ux = b z metodo obratnega vstavljanja. Matrika U je zgornje trikotna.
    """
    m, n = U.shape
    x = np.zeros((n, 1))
    for i in range(n-1, -1, -1):
        pass
        x[i] = (b[i] - U[i, i+1:].dot(x[i+1:].T[0]))/U[i, i]
    return x
    

def gauss_el(A, b):
    """
    Z Gaussovo eliminacijo preoblikuje matriko A in vektor b pri čemer uporabi delno pivotiranje.
    """
    m, n = A.shape
    #print(m, n)
    b = b.astype(float)
    A = A.astype(float)
    
    A_res = A.copy()
    b_res = b.copy()

    P = np.diag(np.ones(n))
        
    for k in range(0, m-1):

        #delno pivotiranje
        max_vr_st = np.where(A[k:, k:] == max(A[k:, k:].max(), A[k:, k:].min(), key = abs))
        max_vr = max_vr_st[0][0] + k
        
        P[[k, max_vr], :] = P[[max_vr, k], :]

        A_res[[k, max_vr], :] = A_res[[max_vr, k], :]
        b_res[[k, max_vr], :] = b_res[[max_vr, k], :]
#        A_res = P.dot(A_res)
#        b_res = P.dot(b_res)
        
        for i in range(k+1, m): # po vrsticah, od naslednje do zadnje
            kvoc = A_res[i, k]/A_res[k, k]
            for j in range(k, n):#gre po stolpcih
                A_res[i, j] = A_res[i, j] - kvoc*A_res[k, j]
            
            b_res[i]= b_res[i] - kvoc*b_res[k]
        
    return A_res, b_res, P



def vector_norm_second(v):
    """
    Druga norma vektorja v.
    """
    return np.linalg.norm(v, 2)

def newton_method_sistem(F, x0, epsilon, J, max_num_of_it = 100):
    """
    Reši sistem enačb zapisan v vektorju F začetnim približkom x0 na natančnost epsilon
    z uporabo Newotonove metode za reševanje sistemov.
    """
    # x je vrstica
    x = x0
    Fx = np.array([fun(*x) for fun in F])
    
    num_of_it = 0
    while vector_norm_second(Fx) >= epsilon and num_of_it < max_num_of_it:
        
        Jx = np.array([[dfun(*x) for dfun in vr] for vr in J])
        A = Jx
        
        #iz vrstice v stolpec
        b = -Fx.reshape(-1, 1)
        
        A_upper_triangular, b, _ = gauss_el(np.array(A), np.array(b))
        delta_x = obratno_vstavljanje(A_upper_triangular, b) #tole je stolpec
        
        x = delta_x.reshape(1, -1)[0] + x #to je vrstica
        #print("x:", x)
        Fx = np.array([fun(*x) for fun in F])
        num_of_it += 1
    
    if vector_norm_second(Fx) > epsilon:
        return None
    else:
        return x


def gradient_descent(f, x0, step, eps, df, method = "minimum"):
    """
    Z metodo gradientnega spusta izračuna extrem (minimum, če argument method = "minimum", drugače izračuna maximum)
    funkcije f z začetnim približkom x0 na absolutno natančnost eps.
    """
    x = x0
    k = df(x)

    if method == "minimum":
        sign = -1
    else:
        sign = 1
        
    x_new = x + sign*step*k
    while abs(x_new - x) >= eps:
        x = x_new
        k = df(x)
        x_new = x + sign*step*k
        
    return x_new

def romberg(f, a, b, eps):
    """
    Z uporabo Rombergove metode izračuna vrednost integrala funkcije f na območju med a in b
    na absolutno natančnost eps.
    Metoda je optimizarana, saj namesto celotne tabele hrani le trenutno in prejšnjo vrstico tabele.
    """
    h = b - a
    T = [h*(f(a) + f(b))/2]

    m = 2
    while True:
        h = h/2
        T_new = [0]*m
        T_new[0] = T[0]/2
        s = 0
        for i in range(1, 2**(m-2) + 1):
            s += f(a + (2*i -1)*h)
        T_new[0] += h*s
        for n in range(2, m+1):
            T_new[n-1] = (4**(n-1)*T_new[n-2] - T[n-2])/(4**(n-1)-1)
      
        if abs(T_new[-1]-T[-1]) < eps: # za absolutno vrednost napake
            return T_new[-1], m
        
        T = T_new.copy()
        m+=1



# ********************************************* G R A F I    I N     A N I M A C I J E *************************************
    
    
def plot_hypotrochoida(a, b, ts, color = "skyblue", alpha = 0.5):
    """
    Izriše graf hypotrochoide po točkah, kjer točke računa s časi podanimi v seznamu ts.
    """
    xes = [Xt(a, b, t) for t in ts]
    yons = [Yt(a, b, t) for t in ts]
    
    plt.plot(xes, yons, color, linewidth = 1.8)
    plt.axhline(y = 0, color = "black", alpha = alpha)
    plt.axvline(x = 0, color = "black", alpha = alpha)

def animation_hypotrochoida(a, b, ts, color = "black", wait = 0.1, title = True):
    """
    Izriše graf hypotrochoide. Nato na obstoječ graf še enkrat riše hypotrochoido po točkah, 
    kjer točke računa s časi podanimi v seznamu ts. Točke riše v razmiku wait sekund. 
    Če title = True v naslov grafa napiše trenuten čas.
    """
    plot_hypotrochoida(a, b, ts)
    plot_hypotrochoida_by_points(a, b, ts, title = title, wait = wait, color = color)
    
def plot_hypotrochoida_by_points(a, b, ts, color = "black", wait = 0.02, title = False):
    """
    Izriše graf hypotrochoide po točkah, kjer točke računa s časi podanimi v seznamu ts. Točke riše v razmiku wait sekund. 
    Če title = True v naslov grafa napiše trenuten čas.
    """
    plt.xlim(-3, 3)
    plt.ylim(-3, 3)
    for t in ts:
        plot_point(a, b, t, color)
        plt.pause(wait)
        if title:
            plt.title("t = " + str(round(t, 2)))
        
def plot_point(a, b, t, color = "red"):
    """
    Izriše točko hypotrochoide ob času t.
    """
    x = Xt(a, b, t)
    y = Yt(a, b, t)
    plt.plot(x, y, color, marker = ".")
    
def plot_point_with_text(a, b, t, i, color = "red"):
    """
    Izriše točko hypotrochoide ob času t ter jo oštevilči.
    """
    x = Xt(a, b, t)
    y = Yt(a, b, t)
    plt.plot(x, y, color, marker = "o")
    plt.text(x, y, "p{}".format(i))
    
def plot_circle(radius, color = "orange"):
    """
    Nariše krog okrog (0, 0) z radijem radius.
    """
    circle = plt.Circle((0, 0), radius, color = color, fill=False, linewidth = 1.8)
    plt.gcf().gca().add_artist(circle)
    

def plot_aprox_intersections(a, b, t_pairs_aprox, color = "blue"):
    """
    Sprejme seznam t_pairs, ki je oblike 
    [[t1_aprox_for_p1, t2_aprox_for_p1], [t1_aprox_for_p2, t2_aprox_for_p2], ..., [t1_aprox_for_p7, t2_aprox_for_p7]],
    kjer časa v posameznem podseznamu predstavljata dober približek za prvi in drugi obhod krivulje čez določeno presečišče.
    Točke krivulje ob vseh teh časih nariše na graf.
    """
    for t1, t2 in t_pairs_aprox:
        plot_point(a, b, t1, color = color)
        plot_point(a, b, t2, color = color)
    
def plot_boxes(a, b, t_pairs, box_width = 0.2, color = "blue"):
    """
    Sprejme seznam t_pairs, ki je oblike 
    [[t1_aprox_for_p1, t2_aprox_for_p1], [t1_aprox_for_p2, t2_aprox_for_p2], ..., [t1_aprox_for_p7, t2_aprox_for_p7]],
    kjer časa v posameznem podseznamu predstavljata dober približek za prvi in drugi obhod krivulje čez določeno presečišče.
    Okrog vsakega prvega časa v podseznamu nariše škatlo širine box_width.
    """
    for t1, t2 in t_pairs:
        x = Xt(a, b, t1) - box_width/2
        y = Yt(a, b, t1) - box_width/2
        rectangle = plt.Rectangle((x, y), box_width, box_width,fill=None, color = color)
        plt.gca().add_patch(rectangle)


def plot_intersections(a, b, t_pairs, color = "red"):
    """
    Nariše zunanja presečišča krivulje na graf.
    """
    for t_idx in range(0, len(t_pairs)):
        plot_point_with_text(a, b, t_pairs[t_idx][0], t_idx + 1, color)


def fill_area(a, b, t_pairs, color = "black", alpha = 0.3, below = 0, above = Yt):
    """
    Zapolni območje med krivuljama below in above.
    """
    for t_pair in t_pairs:
        if t_pair[0] < t_pair[1]:
            ts = np.arange(t_pair[0], t_pair[1] + 0.1, 0.1)
        else:
            ts = np.arange(t_pair[0], t_pair[1] - 0.1, -0.1)
        
        if below == Xt:
            below = Xt(a, b, ts)
        elif below == Yt:
            below = Yt(a, b, ts)
            
        if above == Xt:
            above = Xt(a, b, ts)
        elif above == Yt:
            above = Yt(a, b, ts)
            
        plt.fill_between(Xt(a, b, ts), Yt(a, b, ts), where = below <= above, edgecolor = color,alpha = 1, hatch= "/", facecolor="none", linewidth=0.0)
        
def plot_boundary_curve(a, b, t_pairs, color = "black", points = True, wait = 0.05):
    """
    S pomočjo t_pairs, ki je seznam oblike [[t_p1_out, t_p2_in], [t_p2_out, t_p3_in], ..., [t_p7_out, t_p1_in]], kjer
    vsak podseznam vsebuje začetni in končni čas poteka krivulje od presečišča i do presečišča i + 1,
    izriše zunanji rob hypotrochoide. Če argument points = True zunanji rob krivulje najprej nariše postopoma,
    kjer točke riše v razmiku wait sekund.
    """
    for t_pair in t_pairs:
        if t_pair[0] < t_pair[1]:
            ts = np.arange(t_pair[0], t_pair[1] + 0.1, 0.1)
        else:
            ts = np.arange(t_pair[0], t_pair[1] - 0.1, -0.1)
            
        if points:  
            plot_hypotrochoida_by_points(a, b, ts, color, wait = wait)
        plot_hypotrochoida(a, b, ts, color, alpha = 0.1)


def graph1(a, b):
    plt.clf()
    
    ts = np.arange(0, 70.01, 0.1)
    plot_hypotrochoida(a, b, ts)
    
    plt.savefig("../../../doc/src/domace/02/rkozelj/images_hypotrochoida/hypotrochoida")
    
def graph2(a, b):
    plt.clf()
    
    ts = np.arange(0, 70.01, 0.1)
    plot_hypotrochoida(a, b, ts, alpha = 0)
    
    #najdem dobre približke za presešiča na robnem delu hypotrochoide
    aproximative_t_pairs_for_intersections = find_aproximative_t_pairs_for_intersections(a, b)
 
    #z najdenimi približki, z uporabo newtonove metode za reševanje sistema enačb, izračunam presečišča, ki pa vsebujejo tudi dvojnike
    #(saj več parov približkov presečišč skonvergira k istemu presečišču)
    t_pairs_intersections = find_intersections_with_newton_sistem(a, b, aproximative_t_pairs_for_intersections, epsilon)
    
    #izbrišem dvojnike
    t_pairs_intersections = remove_duplicates(t_pairs_intersections)
    
    #presečišča sortiram tako, da so razvrščena v smeri urinega kazalca z začetkom pri negativnem delu x osi
    t_pairs_intersections = sort_times(a, b, t_pairs_intersections)
    
    #iz izračunanih presečišč (ker vsako presečišče trenutno definirata dva časa (oba časa ob katerih krivulja seka samo sebe na določeni točki)
    #izluščim tiste, s pomočjo katerih se lahko sprehodim po zunanjem robu hypotrochoide v smeri urinega kazalca
    #(izmed temi potem izbiram po katerih intervalih integriram)
    #(če se nariše potek hypotrochoide na graf med začetnim in končnim časom iz vsakega podseznama potem se izriše celotno zun območje hypotrochoide)
    t_pairs_of_boundary_curve = find_t_pairs_of_boundary_curve(a, b, t_pairs_intersections)
  
    #narišem zunanji rob hypotrochoide
    plot_boundary_curve(a, b, t_pairs_of_boundary_curve, points = False, wait = 0)
    plt.savefig("../../../doc/src/domace/02/rkozelj/images_hypotrochoida/hypotrochoida_boundary")
    
    
def graph3(a, b):
    plt.clf()
    
    ts = np.arange(0, 70.01, 0.1)
    plot_hypotrochoida(a, b, ts)
    
    #najdem dobre približke za presešiča na robnem delu hypotrochoide
    aproximative_t_pairs_for_intersections = find_aproximative_t_pairs_for_intersections(a, b)
    #narišem tako velik krog da zunaj njega ostanejo le robna presečišča hypotrochoide
    plot_circle(2)
    #najdene približke narišem na graf
    plot_aprox_intersections(a, b, aproximative_t_pairs_for_intersections)
    #okrog vsakega para približkov narišem box v katerega sta se ujela
    plot_boxes(a, b, aproximative_t_pairs_for_intersections)
    
    plt.savefig("../../../doc/src/domace/02/rkozelj/images_hypotrochoida/hypotrochoida_aprox")
    

def graph4(a, b):
    plt.clf()
    
    ts = np.arange(0, 70.01, 0.1)
    plot_hypotrochoida(a, b, ts)
    
    #najdem dobre približke za presešiča na robnem delu hypotrochoide
    aproximative_t_pairs_for_intersections = find_aproximative_t_pairs_for_intersections(a, b)
    
    #z najdenimi približki, z uporabo newtonove metode za reševanje sistema enačb, izračunam presečišča, ki pa vsebujejo tudi dvojnike
    #(saj več parov približkov presečišč skonvergira k istemu presečišču)
    t_pairs_intersections = find_intersections_with_newton_sistem(a, b, aproximative_t_pairs_for_intersections, epsilon)
    
    #izbrišem dvojnike
    t_pairs_intersections = remove_duplicates(t_pairs_intersections)
    
    #presečišča sortiram tako, da so razvrščena v smeri urinega kazalca z začetkom pri negativnem delu x osi
    t_pairs_intersections = sort_times(a, b, t_pairs_intersections)
    
    #narišem sortirana presečišča
    plot_intersections(a, b, t_pairs_intersections)
    
    plt.savefig("../../../doc/src/domace/02/rkozelj/images_hypotrochoida/hypotrochoida_intersections")
    
        
if __name__ == "__main__":
    a = 1
    b = -11/7
    epsilon = 10**(-10)
    
    #a = 2
    
    ts = np.arange(0, 70.01, 0.1)
    #plot_hypotrochoida(a, b, ts)
    #plot_hypotrochoida_by_points(a, b, ts)
    
    area = area_hypotrochoida(a, b, epsilon, direction = "x", animated = True)
    
    #animation_hypotrochoida(a, b, ts)
    
#    graph1(a, b)
#    graph2(a, b)
#    graph3(a, b)
#    graph4(a, b)
    
    
    
    

    
    
