# naj asistent ignorira spodnji dve vrstici
#cd("D:\\git\\Num-mat\\DN\\dn2\\nm-dn2\\src\\domaca02\\rkozelj")
#include("porazdelitvena.jl")

using SpecialFunctions

"""
    f(x)

Funkcija gostote verjetnosti.
"""
function f(x)
    return exp((-x^2)/(2*pi))/sqrt(2*pi)
end

"""
    cumulative_distribution_function_exact(x)

Izračuna eksaktno vrednost integrala funkcije gostote verjetnosti `f(x)` v mejah od -Inf do `x` oziroma
eksaktno vrednost funkcije porazdelitvene verjetnosti za dan `x`.
"""
function cumulative_distribution_function_exact(x)
    return 1/2 * sqrt(pi) * (erf(x/sqrt(2*pi))+1)
end

"""
    whole_area()

Vrne vrednost integrala funkcije gostote verjetnosti `f(x)` v mejah od -Inf do Inf oziroma 
vrednost funkcije porazdelitvene verjetnosti za x = Inf.
"""
function whole_area()
    return sqrt(pi)
end

"""
    first_half_area()

Vrne vrednost integrala funkcije gostote verjetnosti `f(x)` v mejah od -Inf do 0 oziroma 
vrednost funkcije porazdelitvene verjetnosti za x = 0.
"""
function first_half_area()
    return sqrt(pi)/2
end

"""
    insert_xs_and_weights(n_legendre, n_laguerre)

Požene python skripto `calculate_and_write_xs_and_weights.py`, ki za podana `n_legendre` in `n_laguerre` izračuna 
ničle in uteži Gauss-Legendrovih in/ali Gauss-Laguerrovih polinomov ter jih zapiše v datoteko xs_and_weights.txt.
Argumenta morata biti oba celi števili ali pa je eden celo število drugi pa string "no value", pri čemer
se ničle in uteži izračunajo le za tistega, ki je integer.
"""
function insert_xs_and_weights(n_legendre, n_laguerre)
    run(`python calculate_and_write_xs_and_weights.py $n_legendre $n_laguerre`)
end


"""
    simpson13(f, a, b, n)

Z uporabo simpsonove 1/3 metode izračuna vrednost integrala funkcije `f` v mejah od `a` do `b` z `n` intervali.
"""
function simpson13(f, a, b, n)
    h = (b-a)/(2*n)
    I = f(a) + 4*f(a + h)
    for i = 1:n-1
        I += 2*f(a + 2*i*h) + 4*f(a + (2*i + 1)*h)
    end
    I += f(b)

    return I*h/3
end

"""
    simpson13_value_and_num_of_intervals(x, epsilon)

Z uporabo simpsonove 1/3 metode izračuna 
    - vrednost integrala funkcije gostote verjetnosti `f(x)` v mejah od -Inf do `x`, z absolutno natančnostjo `epsilon` ter 
    - najmanjše zadostno število korakov za dovolj natančen izračun.

Primer uporabe
    value, number_of_intervals = simpson13_value_and_num_of_intervals(3, 10^(-10))
"""
function simpson13_value_and_num_of_intervals(x, epsilon)
    if x >= 15
        return sqrt(pi), 0
    elseif x <= -15
        return 0, 0
    end

    a = 0
    b = x

    n = 1
    I1 = simpson13(f, a, b, n) + first_half_area()
    n = 2*n
    I2 = simpson13(f, a, b, n) + first_half_area()

    #while abs(abs(I1 - I2)/I2) >= epsilon # relativna natančnost 10^(-10)
    while abs(I1 - I2) >= epsilon # natančnost na 10 decimalk
        I1 = I2
        n = 2*n
        I2 = simpson13(f, a, b, n) + first_half_area()
    end
    return I2, n
end

"""
    romberg_value_and_size_of_the_table(x, epsilon)

Z uporabo Rombergove metode izračuna 
    - vrednost integrala funkcije gostote verjetnosti `f(x)` v mejah od -Inf do `x`, z absolutno natančnostjo `epsilon` ter 
    - najmanjšo velikost Rombergove tabele za dovolj natančen izračun.
Metoda je optimizarana, saj namesto celotne tabele hrani le trenutno in prejšnjo vrstico tabele.

Primer uporabe
    value, size_of_the_table = romberg_value_and_size_of_the_table(3, 10^(-10))
"""
function romberg_value_and_size_of_the_table(x, epsilon)
    if x >= 15
        return sqrt(pi), 0
    elseif x <= -15
        return 0, 0
    end

    a = 0
    b = x

    h = b-a
    T = [h*(f(a) + f(b))/2]
    m = 2
    while true
        h = h/2

        T_new = [0.0 for i in 1:m]
        T_new[1] = T[1]/2
        s = 0
        for i = 1:2^(m-2)
            s += f(a + (2*i - 1)*h)
        end
        T_new[1] += h*s

        for n = 2:m
            T_new[n] = (4^(n-1)*T_new[n-1] -  T[n-1])/(4^(n-1)-1)
        end

        T_last = T_new[end] + first_half_area() 
        T_second_last = T[end] + first_half_area()

        #if abs(abs(T_last-T_second_last)/T_last) < epsilon # relativna natančnost 10^(-10)
        if abs(T_last-T_second_last) < epsilon # natančnost na 10 decimalk
            return T_last, m
        end

        T = copy(T_new)
        m += 1

    end
end


"""
    gauss_quadrature(f, a, b, order_of_polynomial) 

Z uporabo Gaussovih kvadratur izračuna vrednost integrala funkcije `f` v mejah od 
`a` do `b`, kjer funkcijo `f` aproksimira s polinomom stopnje `order_of_polynomial`.
"""
function gauss_quadrature(f, a, b, order_of_polynomial)
    if order_of_polynomial % 2 == 0
        println("Only odd degree polynomials are alowed.")
    end

    n = Int((order_of_polynomial + 1)/2)

    n_legendre, n_laguerre = n, "no value"
    insert_xs_and_weights(n_legendre, n_laguerre)
    xs_legendre, weights_legendre = read_from_file("xs_and_weights.txt", "xs_legendre", "weights_legendre")

    I = sum([f((b - a)/2*xs_legendre[i] + (b + a)/2)*weights_legendre[i] for i in 1:length(xs_legendre)])

    return (b-a)/2 * I
end

"""
    gauss_quadrature_value_and_optimal_order(x, epsilon, integral_value)  

Z uporabo Gaussovih kvadratur izračuna 
    - vrednost integrala funkcije gostote verjetnosti `f(x)` v mejah od -Inf do `x` ter 
    - najnižjo stopnjo aproksimacijskega polinoma, da je integral izračun z absolutno natančnostjo `epsilon`. 
    
Najnižjo stopnjo aproksimacijskega polinoma izračuna tako, da `f(x)` na vsakem koraku aproksimira s polinomom 
čedalje višje stopnje (pri čemer upošteva le lihe stopnje) ter izračuna absolutno razliko med dobljeno vrednostjo ter 
`integral_value`, ki že predstavlja dovolj natančno izračunano vrednost funkcije porazdelitvene verjetnosti po neki drugi metodi.
`integral_value` je lahko vrednost funkcije porazdelitvene verjetnosti izračunane po Simpsonovi 1/3 ali Rombergovi metodi ali pa kar eksaktna vrednost.

Primer uporabe
    integral_value, size_of_the_table = romberg_value_and_size_of_the_table(3, 10^(-10))
    value, order = gauss_quadrature_value_and_optimal_order(3, 10^(-10), integral_value)
"""
function gauss_quadrature_value_and_optimal_order(x, epsilon, integral_value)
    if x >= 15
        return sqrt(pi), 0
    elseif x <= -15
        return 0, 0
    end
 
    a = 0
    b = x

    order = 1
    I = gauss_quadrature(f, a, b, order) + first_half_area()

    #while abs(abs(I - integral_value)/integral_value) >= epsilon # relativna natančnost 10^(-10)
    while abs(I - integral_value) >= epsilon # natančnost na 10 decimalk
        order += 2
        I = gauss_quadrature(f, a, b, order) + first_half_area()
    end

    return I, order
end

"""
    gauss_laguerre_quadrature(f, n, x) 

Z uporabo Gauss Laguerrovih kvadratur izračuna vrednost integrala funkcije `f` v mejah od -`x` do Inf,
kjer funkcijo `f` aproksimira s polinomom stopnje `n`.
"""
function gauss_laguerre_quadrature(f, n, x)
    function g(x)
        return f(x)*exp(x)
    end

    n_legendre, n_laguerre = "no value", n
    insert_xs_and_weights(n_legendre, n_laguerre)
    xs_laguerre, weights_laguerre = read_from_file("xs_and_weights.txt", "xs_laguerre", "weights_laguerre")
    
    I = sum([g(xs_laguerre[i]-x)*weights_laguerre[i] for i in 1:length(xs_laguerre)])

    return exp(x)*I
    
end

"""
    gauss_laguerre_quadrature_value_and_optimal_order(x, epsilon, integral_value) 

Z uporabo Gauss Laguerrovih kvadratur izračuna 
    - vrednost integrala funkcije gostote verjetnosti `f(x)` v mejah od -Inf do `x`, ter 
    - najnižjo stopnjo aproksimacijskega polinoma, da je integral izračun z absolutno natančnostjo `epsilon`. 
    
Najnižjo stopnjo aproksimacijskega polinoma izračuna tako, da `f(x)` na vsakem koraku aproksimira s polinomom 
čedalje višje stopnje ter izračuna absolutno razliko med dobljeno vrednostjo ter `integral_value`, ki že predstavlja 
dovolj natančno izračunano vrednost funkcije porazdelitvene verjetnosti po neki drugi metodi.
`integral_value` je lahko vrednost porazdelitvene funkcije izračunane po Simpsonovi 1/3 ali Rombergovi metodi ali pa kar eksaktna vrednost.
Za izračun integrala `f(x)`, aproksimirane s polinomom stopnje n, se uporablja funkcija `gauss_laguerre_quadrature(f, n, x)`.

Primer uporabe
    integral_value, size_of_the_table = romberg_value_and_size_of_the_table(3, 10^(-10))
    value, order = gauss_laguerre_quadrature_value_and_optimal_order(3, 10^(-10), integral_value)
"""
function gauss_laguerre_quadrature_value_and_optimal_order(x, epsilon, integral_value)
    if x >= 15
        return sqrt(pi), 0
    elseif x <= -15
        return 0, 0
    end

    n = 1
    I = gauss_laguerre_quadrature(f, n, x)

   # while abs(abs(I - integral_value)/integral_value) >= epsilon # relativna natančnost 10^(-10)
    while abs(I - integral_value) >= epsilon # natančnost na 10 decimalk
        n += 1
        I = gauss_laguerre_quadrature(f, n, x)
    end
    return I, n
end

"""
    gauss_laguerre_quadrature_better(f, n, x) 

Z uporabo Gauss Laguerrovih kvadratur izračuna vrednost integrala funkcije `f` v mejah od `x` do Inf,
kjer funkcijo `f` aproksimira s polinomom stopnje `n`. Od funkcije `gauss_laguerre_quadrature(f, n, x)`
se razlikuje v tem, da integral vedno izračuna na območju med pozitivnim `x` do Inf in v primeru, ko `x` manjši od 0,
pravo vrednost dobi tako, da od vrednosti `whole_area` (integral funkcije `f` na območju od -Inf do Inf) odšteje dobljeno vrednost.
"""
function gauss_laguerre_quadrature_better(f, n, x, whole_area)
    function g(x)
        return f(x)*exp(x)
    end

    n_legendre, n_laguerre = "no value", n
    insert_xs_and_weights(n_legendre, n_laguerre)
    xs_laguerre, weights_laguerre = read_from_file("xs_and_weights.txt", "xs_laguerre", "weights_laguerre")

    I = sum([g(xs_laguerre[i]+abs(x))*weights_laguerre[i] for i in 1:length(xs_laguerre)])

    if x < 0
        return exp(-abs(x))*I
    else
        return whole_area - exp(-abs(x))*I
    end
    
end

"""
    gauss_laguerre_quadrature_value_and_optimal_order_better(x, epsilon, integral_value) 

Z uporabo Gauss Laguerrovih kvadratur izračuna 
    - vrednost integrala funkcije `f(x)` v mejah od -inf do `x`, ter 
    - najnižjo stopnjo aproksimacijskega polinoma, da je integral izračun z absolutno natančnostjo `epsilon`. 
    
Najnižjo stopnjo aproksimacijskega polinoma izračuna tako, da `f(x)` na vsakem koraku aproksimira s polinomom 
čedalje višje stopnje ter izračuna absolutno razliko med dobljeno vrednostjo ter `integral_value`, ki že predstavlja 
dovolj natančno izračunano vrednost funkcije porazdelitvene verjetnosti po neki drugi metodi.
`integral_value` je lahko vrednost funkcije porazdelitvene verjetnosti izračunane po Simpsonovi 1/3 ali Rombergovi metodi ali pa kar eksaktna vrednost.
Of funkcije `gauss_laguerre_quadrature_value_and_optimal_order(x, epsilon, integral_value)` se razlikuje v tem, 
da se za izračun integrala `f(x)`, aproksimirane s polinomom stopnje n, uporablja funkcija `gauss_laguerre_quadrature_better(f, n, x, whole_area)`.

Primer uporabe
    integral_value, size_of_the_table = romberg_value_and_size_of_the_table(3, 10^(-10))
    value, order = gauss_laguerre_quadrature_value_and_optimal_order_better(3, 10^(-10), integral_value)
"""
function gauss_laguerre_quadrature_value_and_optimal_order_better(x, epsilon, integral_value)
    if x >= 15
        return sqrt(pi), 0
    elseif x <= -15
        return 0, 0
    end

    n = 1
    I = gauss_laguerre_quadrature_better(f, n, x, whole_area())
    #while abs(abs(I - integral_value)/integral_value) >= epsilon # relativna natančnost 10^(-10)
    while abs(I - integral_value) >= epsilon # natančnost na 10 decimalk
        n += 1
        I = gauss_laguerre_quadrature_better(f, n, x, whole_area())
    end
    return I, n
end

"""
    cumulative_distribution_function_numerical(x, epsilon; integral_value = nothing, method = nothing)

Funkcija z uporabo izbrane metode numeričnega integriranja izračuna 
    - vrednost integrala funkcije gostote verjetnosti `f(x)` v mejah od -Inf do `x` ter
    v primeru ko method = "simpson13"
    - najmanjše zadostno število korakov, da je integral izračunan z absolutno natančnostjo `epsilon`.
    v primeru ko method = "romberg"
    - najmanjšo velikost Rombergove tabele, da je integral izračunan z absolutno natančnostjo `epsilon`.
    v primeru ko method = "gauss", "gauss laguerre" ali "gauss laguerre better"
    - najnižjo stopnjo aproksimacijskega polinoma, da je integral izračunan z absolutno natančnostjo `epsilon`.
    pri čemer je potrebno pri zadnjih treh metodah podati tudi argument `integral_value`, ki že predstavlja
    dovolj natančno izračunano vrednost funkcije porazdelitvene verjetnosti po neki drugi metodi ali pa kar eksaktno vrednost.

Primer uporabe
    I_romberg, size_of_the_table_romberg = cumulative_distribution_function_numerical(x, epsilon; method = "romberg")
    I_gauss_laguerre, order_gauss_laguerre = cumulative_distribution_function_numerical(x, epsilon;  integral_value = I_romberg, method = "gauss laguerre")
"""
function cumulative_distribution_function_numerical(x, epsilon; integral_value = nothing, method = nothing)
    if method == "simpson13"
        return simpson13_value_and_num_of_intervals(x, epsilon)
    elseif method == "romberg"
        return romberg_value_and_size_of_the_table(x, epsilon)
    elseif method == "gauss"
        return gauss_quadrature_value_and_optimal_order(x, epsilon, integral_value)
    elseif method == "gauss laguerre"
        return gauss_laguerre_quadrature_value_and_optimal_order(x, epsilon, integral_value)
    elseif method == "gauss laguerre better"
        return gauss_laguerre_quadrature_value_and_optimal_order_better(x, epsilon, integral_value)
    end
end



"""
    read_from_file(file_name, xs, weights)

Prebere datoteko z imenom oziroma relativno potjo `file_name`. 
V primeru ko xs = "xs_legendre" in weights = "weights_legendre" prebere ničle in uteži Gauss Legendrovih polinomov,
v primeru ko xs = "xs_laguerre" in weights = "weights_leguerre" pa ničle in uteži Gauss Laguerrovih polinomov.
"""
function read_from_file(file_name, xs, weights)
    res_xs = nothing
    res_weights = nothing
    open(file_name) do f
        for line in eachline(f)
            name, res = split(line, " = ")
            if name == weights
                res_weights = eval(Meta.parse(strip(res)))
            elseif name == xs
                res_xs = eval(Meta.parse(strip(res)))
            end
        end
    end
    return res_xs, res_weights
end




"""
    test(x_start, x_end)

Funkcija uporabljena za testiranje zgoraj napisanih funkcij in izpis rezultatov. 
Funkcije testira s parametrom `x` ki zavzame vrednosti med `start_x` in `end_x`.
"""
function test(x_start, x_end)
    epsilon = 10^(-10)

    for x= x_start:x_end
        println("x ", x)
        I = cumulative_distribution_function_exact(x)
        #integral_value = cumulative_distribution_function_exact(x)

        #I_simpson13, num_of_intervals_simpson13 = simpson13_value_and_num_of_intervals(x, epsilon)
        I_simpson13, num_of_intervals_simpson13 = cumulative_distribution_function_numerical(x, epsilon; method = "simpson13")

        #I_romberg, size_of_the_table_romberg = romberg_value_and_size_of_the_table(x, epsilon)
        I_romberg, size_of_the_table_romberg = cumulative_distribution_function_numerical(x, epsilon; method = "romberg")

        #I_gauss, order_gauss = gauss_quadrature_value_and_optimal_order(x, epsilon, I_romberg)
        I_gauss, order_gauss = cumulative_distribution_function_numerical(x, epsilon;  integral_value = I_romberg, method = "gauss")

        #I_gauss_laguerre, order_gauss_laguerre = gauss_laguerre_quadrature_value_and_optimal_order(x, epsilon, I_romberg))
        I_gauss_laguerre, order_gauss_laguerre = cumulative_distribution_function_numerical(x, epsilon;  integral_value = I_romberg, method = "gauss laguerre")

        #I_gauss_laguerre_better, order_gauss_laguerre_better = gauss_laguerre_quadrature_value_and_optimal_order_better(x, epsilon, I_romberg))
        I_gauss_laguerre_better, order_gauss_laguerre_better = cumulative_distribution_function_numerical(x, epsilon; integral_value = I_romberg, method = "gauss laguerre better")

        #i zpis vrednosti funkcij
        println("exact ", I)
        println("simpson13:", I_simpson13, ", ", num_of_intervals_simpson13)
        println("romberg:", I_romberg, ", ", size_of_the_table_romberg)
        println("gauss find order:", I_gauss, ", ", order_gauss)
        println("gauss laguerre find order:", I_gauss_laguerre, ", ", order_gauss_laguerre)
        println("gauss laguerre find order better:", I_gauss_laguerre_better, ", ",  order_gauss_laguerre_better)

        # testiranje z absolutno napako
        println("simpson13:", abs(I_simpson13 - I) < epsilon)
        println("romberg:", abs(I_romberg - I) < epsilon)
        println("gauss find order:", abs(I_gauss- I) < epsilon)
        println("gauss laguerre find order:", abs(I_gauss_laguerre - I) < epsilon)
        println("gauss laguerre find order better:", abs(I_gauss_laguerre_better - I) < epsilon)

        println("simpson13:", abs(I_simpson13 - I))
        println("romberg:", abs(I_romberg - I))
        println("gauss find order:", abs(I_gauss- I))
        println("gauss laguerre find order:", abs(I_gauss_laguerre - I))
        println("gauss laguerre find order better:", abs(I_gauss_laguerre_better - I))

       # testiranje v primeru implementacije natančnosti z relativno napako
       # println("simpson13:", abs(abs(I_simpson13 - I)/I) < epsilon)
       # println("romberg:", abs(abs(I_romberg - I)/I) < epsilon)
       # println("gauss find order:", abs(abs(I_gauss - I)/I) < epsilon)
       # println("gauss laguerre find order:", abs(abs(I_gauss_laguerre- I)/I) < epsilon)
       # println("gauss laguerre find order better:", abs(abs(I_gauss_laguerre_better - I)/I) < epsilon)

       # println("simpson13:", abs(abs(I_simpson13 - I)/I))
       # println("romberg:", abs(abs(I_romberg - I)/I))
       # println("gauss find order:", abs(abs(I_gauss_find_order - I)/I))
       # println("gauss laguerre find order:", abs(abs(I_gauss_laguerre - I)/I))
       # println("gauss laguerre find order better:", abs(abs(I_gauss_laguerre_better - I)/I))

    end

end

x_start = -14
x_end = 14

#@elapsed test(x_start, x_end)

