# naj asistent ignorira spodnji dve vrstici
#cd("D:\\git\\Num-mat\\DN\\dn2\\nm-dn2\\doc\\src\\domace\\02\\rkozelj")
#include("plots.jl")

include("..\\..\\..\\..\\..\\src\\domaca02\\rkozelj\\porazdelitvena.jl")

using Plots

"""
    plot_order_of_polynomial(x_start, x_end, epsilon)

Izriše graf najnižjih stopenj aproksimaicjskih polinomov uporabljenih pri Gauss-Legendrovih
ter obeh Gauss-Laguerrovih metodah za izračun funkcije porazdelitvene verjetnosti za dan `x`, ki se nahaja na intervalu med `x_start` in `x_end` 
z absolutno natančnostjo `epsilon` v odvisnosti od x.
"""
function plot_order_of_polynomial(x_start, x_end, epsilon)
    xes = [i for i in x_start:x_end]
    len = x_end - x_start + 1
    order_gauss_list = [0.0 for i in 1:len]
    order_gauss_laguerre_list = [0.0 for i in 1:len]
    order_gauss_laguerre_better_list = [0.0 for i in 1:len]

    for i = 1:len
        x = xes[i]
        println("x ", x)

        I_romberg, size_of_the_table_romberg = cumulative_distribution_function_numerical(x, epsilon; method = "romberg")
        
        _, order_gauss = cumulative_distribution_function_numerical(x, epsilon; integral_value = I_romberg, method = "gauss")
        order_gauss_list[i] = order_gauss
        
        _, order_gauss_laguerre = cumulative_distribution_function_numerical(x, epsilon; integral_value = I_romberg, method = "gauss laguerre")
        order_gauss_laguerre_list[i] = order_gauss_laguerre
        
        _, order_gauss_laguerre_better = cumulative_distribution_function_numerical(x, epsilon; integral_value = I_romberg, method = "gauss laguerre better")
        order_gauss_laguerre_better_list[i] = order_gauss_laguerre_better
        
        println("gauss:", order_gauss_list[i])
        println("gauss laguerre:", order_gauss_laguerre_list[i])
        println("gauss laguerre better:", order_gauss_laguerre_better_list[i])
    end

    plot(xes, order_gauss_list, ylabel = "stopnja polinoma", xlabel = "x", markershape = :circle, color = "red", label = "Gauss", legend=:topleft)
    plot!(xes, order_gauss_laguerre_list, markershape = :circle, color = "green", label = "Gauss Laguerre")
    plot!(xes, order_gauss_laguerre_better_list, markershape = :circle, color = "blue", label = "Gauss Laguerre better")
    savefig("images_porazdelitvena\\stopnje.png")
end


"""
    plot_num_of_intervals_and_table_size(x_start, x_end, epsilon)

Podobno kot zgornja funkcija, le da ta izriše graf števila intervalov uporabljenih pri Simpsonovi 1/3 metodi in
graf velikosti Rombergove tabele v odvisnosti od x.
"""
function plot_num_of_intervals_and_table_size(x_start, x_end, epsilon)
    xes = [i for i in x_start:x_end]
    len = x_end - x_start + 1
    num_of_intervals_simpson13_list = [0.0 for i in 1:len]
    size_of_the_tabel_romberg_list = [0.0 for i in 1:len]

    for i = 1:len
        x = xes[i]
        println("x ", x)

        _, num_of_intervals_simpson13 = cumulative_distribution_function_numerical(x, epsilon; method = "simpson13")
        num_of_intervals_simpson13_list[i] = num_of_intervals_simpson13

        _, size_of_the_table_romberg = cumulative_distribution_function_numerical(x, epsilon; method = "romberg")
        size_of_the_tabel_romberg_list[i] = size_of_the_table_romberg
        
        println("simpson13:", num_of_intervals_simpson13_list[i])
        println("romberg:", size_of_the_tabel_romberg_list[i])
    
    end
    
    plot(xes, num_of_intervals_simpson13_list, ylabel = "stevilo intervalov", xlabel = "x", markershape = :circle, color = "red", label = "Simpson13")
    savefig("images_porazdelitvena\\stevilo_intervalov.png")
    
    plot(xes, size_of_the_tabel_romberg_list, ylabel = "velikost tabele", xlabel = "x", markershape = :circle, color = "blue", label = "Romberg", legend=:bottomright)
    savefig("images_porazdelitvena\\velikost_tabele.png")
    
    #plot(xes, num_of_intervals_simpson13_list, ylabel = "stevilo intervalov in velikost tabele", xlabel = "x", markershape = :circle, color = "red", label = "Simpson13")
    #plot!(xes, size_of_the_tabel_romberg_list, markershape = :circle, color = "blue", label = "Romberg")
    #savefig("images_porazdelitvena\\stevilo_intervalov_in_velikost_tabele.png")

end

"""
    plot_time(x_start, x_end, epsilon)

Izriše graf povprečnih časov izvajanja različnih metod numeričnega integriranja
za izračun funkcije porazdelitvene verjetnosti za dan `x`, ki se nahaja na intervalu med `x_start` in `x_end` 
z absolutno natančnostjo `epsilon` v odvisnosti od x.
"""
function plot_time(x_start, x_end, epsilon)
    xes = [i for i in x_start:x_end]
    len = x_end - x_start + 1
    time_simpson13 = [0.0 for i in 1:len]
    time_romberg = [0.0 for i in 1:len]
    time_gauss = [0.0 for i in 1:len]
    time_gauss_laguerre = [0.0 for i in 1:len]
    time_gauss_laguerre_better = [0.0 for i in 1:len]
    
    n_times = 3
    for i = 1:len
        x= xes[i]
        println("x ", x)
          
        time_simpson13[i] = @elapsed for t in 1: n_times
            _, _ = cumulative_distribution_function_numerical(x, epsilon; method = "romberg")
        end
        time_simpson13[i] /= n_times

        time_romberg[i] = @elapsed for t in 1: n_times
            _, _ = cumulative_distribution_function_numerical(x, epsilon; method = "romberg")
        end
        time_romberg[i] /= n_times
        
        I_romberg, _ = romberg_value_and_size_of_the_table(x, epsilon)

        time_gauss[i] = @elapsed for t in 1: n_times
            _, _ = cumulative_distribution_function_numerical(x, epsilon; integral_value = I_romberg, method = "gauss")
        end
        time_gauss[i] /= n_times

        time_gauss_laguerre[i] = @elapsed for t in 1: n_times
            _, _ = cumulative_distribution_function_numerical(x, epsilon; integral_value = I_romberg, method = "gauss laguerre")
        end
        time_gauss_laguerre[i] /= n_times

        time_gauss_laguerre_better[i] = @elapsed for t in 1: n_times
            _, _ = cumulative_distribution_function_numerical(x, epsilon; integral_value = I_romberg, method = "gauss laguerre better")
        end
        time_gauss_laguerre_better[i] /= n_times
    end

    plot(xes, time_simpson13, ylabel = "cas izvajanja [s]", xlabel = "x", markershape = :circle, color = "red", label = "Simpson 13", legend=:topleft)
    plot!(xes, time_romberg, markershape = :circle, color = "green", label = "Romberg")
    plot!(xes, time_gauss, markershape = :circle, color = "blue", label = "Gauss")
    plot!(xes, time_gauss_laguerre, markershape = :circle, color = "magenta", label = "Gauss Laguerre")
    plot!(xes, time_gauss_laguerre_better, markershape = :circle, color = "black", label = "Gauss Laguerre better")
    savefig("images_porazdelitvena\\casi.png")

    plot(xes, time_gauss, ylabel = "cas izvajanja [s]", xlabel = "x", markershape = :circle, color = "blue", label = "Gauss", legend=:topleft)
    plot!(xes, time_gauss_laguerre, markershape = :circle, color = "magenta", label = "Gauss Laguerre")
    plot!(xes, time_gauss_laguerre_better, markershape = :circle, color = "black", label = "Gauss Laguerre better")
    savefig("images_porazdelitvena\\casi_polinomi.png")

    plot(xes, time_simpson13, ylabel = "cas izvajanja [s]", xlabel = "x", markershape = :circle, color = "red", label = "Simpson 13", legend=:topleft)
    plot!(xes, time_romberg, markershape = :circle, color = "green", label = "Romberg")
    savefig("images_porazdelitvena\\casi_simpson_romberg.png")

end




x_start = -14
x_end = 14
epsilon = 10^(-10)

#plot_order_of_polynomial(x_start, x_end, epsilon)
#plot_num_of_intervals_and_table_size(x_start, x_end, epsilon)


#plot_time(x_start, x_end, epsilon)