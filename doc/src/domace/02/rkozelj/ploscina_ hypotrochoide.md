# Ploščina hypotrochoide

V nalogi računam ploščino območja, ki ga omejuje hypotrochoida podana parametrično z enačbama
 ```math
    x(t) = (a + b)\cos{t} + b\cos{\frac{(a + b)}{b}t}
```
```math
    y(t) = (a + b)\sin{t} + b\sin{\frac{(a + b)}{b}t}
```
za parametra $`a=1`$ in $`b=−\frac{11}{7}`$.

Vse metode so implementirane v skrpiti `ploscina_hypotrochoide.py`.

Spodnji levi graf prikazuje izris hypotrochoide pri podanih parametrih, na desnem grafu pa sem s črno črto prikazala rob območja katerega ploščino računam.

<img src="images_hypotrochoida/hypotrochoida.png" alt="hypotrochoida" width="550" height="370"/>
<img src="images_hypotrochoida/hypotrochoida_boundary.png" alt="hypotrochoida_boundary" width="550" height="370"/>

Grafa sta izrisana s funkcijama `graph1(a, b)` in `graph2(a, b)`.

Da sem dobila občutek, kakšen je časovni potek risanja krivulje in koliko časa traja en obhod, sem napisala funkcijo `animation_hypotrochoida(a, b, ts, wait = 0.1, title = True)`. 
Funkcija najprej izriše graf hypotrochoide. Nato na obstoječ graf še enkrat izriše hypotrochoido po točkah, kjer točke računa s časi podanimi v seznamu `ts`. 
Vsaka točka se nariše z zamikom `wait` sekund, pri čemer 
se vsak obiskan čas izpiše v naslov grafa. Opazila sem, da v primeru, ko krivuljo rišem z naraščanjem `t` in začnem pri $`t = 0`$, krivulja prvi obhod zaključi 
približno ob $`t = 70`$.

<!--Najprej sem poiskala dobre prbiližke za čase, ko funkcija seka samo sebe na zunanjen robu. -->
Za izračun ploščine, ki ga omejuje zunanji rob hypotrochoide, sem morala najprej poiskati čase pri katerih hypotrochoida seka samo sebe na zunanjem robu.
Vsakega od teh presečišč krivulja (v enem obhodu) obišče dvakrat in zato vsakemu pripadata dva časa. Najprej sem za vsako presečišče poiskala dobre približke za ta dva časa.
To sem s storila z uporabo 
funkcije `find_aproximative_t_pairs_for_intersections(a, b, t_distance = 5, box_width = 0.2, radius = 2, t_step = 0.3)`.
Ta pregleduje vrednosti hypotrochoide ob časih med $`t = 0`$ in $`t = 70`$, s korakom `t_step`. 
Če je vrednost hypotrochoide ob obiskanem času izven kroga z radijem `radius` (ker iščem le presečišča na zunanjem robu hypotrochoide)
in če vrednost hypotrochoide ob tem času pade v box širine `box_width` okrog katere od že obiskanih točk (te hranim v seznamu imenovanem `t_visited`),
pri čemer sta časa vsaj `t_distance` narazen (ker me zanimata časa, ko krivulja prečka dano presečišče v eni in v drugi smeri),
potem sta ta dva časa dober začetni približek za iskanje presečišča ob katerem se nahajata. Ta dva časa shranim v seznam `aproximative_t_pairs_for_intersections`.
Če vrednost hypotrochoide ob obiskanem času ne pade v box od katere od že obiskanih točk, vendar je vrednost ob tem času izven kroga z radijem `radius`,
čas shranim v seznam `t_visited`.
Vrnjen seznam `aproximative_t_pairs_for_intersections` je oblike `[[t1_aprox_for_p1, t2_aprox_for_p1], [t1_aprox_for_p2, t2_aprox_for_p2], ..., [t1_aprox_for_p7, t2_aprox_for_p7]]`,
kjer so presečišča oštevilčena poljubno (ni vrstnega reda med presečišči). Parametre sem določila z malo poizkušanja ter risanja grafa in animacije.

Na spodnjem grafu prikazujem približke za presečišča, ki sem jih našla s podanimi parametri funkcije. Narisala sem tudi boxe, ki prikazujejo katere točke so se ujele skupaj
ter krog zunaj katerega sem točke iskala.

<img src="images_hypotrochoida/hypotrochoida_aprox.png" alt="hypotrochoida_aprox" width="550" height="370"/>

Graf je izrisan s funkcijo `graph3(a, b)`.

Nato sem napisala funkcijo `find_intersections_with_newton_sistem(a, b, aproximative_t_pairs_for_intersections, epsilon)`, ki z najdenimi približki 
z uporabo Newtonove metode za reševanje sistema enačb za vsak par približkov reši sistem
```math
    (x(t_1​), y(t_1​))=(x(t_2​),y(t_2​)),
```
in najde časa, ko krivulja seka presečišče $`p_i`$, kjer sta začetna približka za ($`t_1`$, $`t_2`$) = ($`t_1`$\_aprox_for\_$`p_i`$, $`t_2`$\_aprox_for\_$`p_i`$).

S tem sem za vsak par približnih časov dobila par časov, ob katerih hypotrochoide seka samo sebe v določeni točki. Te pare časov
sem hranila v seznamu `t_pairs_intersections` iz katerega sem nato odstranila dvojnike, saj več parov približkov presečišč skonvergira k istemu presečišču.
Presečišča sem nato z uporabo funkcije `sort_times(a, b, t_pairs_intersections)` z algoritmom urejanja z vstavljanjem uredila v smeri urinega 
kazalca z začetkom na negativni strani $`x`$ osi. Seznam je bil po tem procesu oblike 
`[[p1_first_time, p1_second_time], [p2_first_time, p2_second_time], ..., [p7_first_time, p7_second_time]]`, kjer vsak podseznam vsebuje prvi in drugi čas, 
ko krivulja prečka $`i`$-to presečišče.
Tako urejena presečišča prikazujem na spodnjem grafu.

<img src="images_hypotrochoida/hypotrochoida_intersections.png" alt="hypotrochoida_intersections" width="550" height="370"/>

Graf je izrisan s funkcijo `graph4(a, b)`.

Nato sem iz seznama `t_pairs_intersections` izluščila čase, ki jih potrebujem za izris roba krivulje v smeri poteka urinega kazalca. V ta namen sem 
napisala funkcijo `find_t_pairs_of_boundary_curve(a, b, t_pairs_intersections)`. Ta izmed zaporednih parov časov, najde tista dva (enaga, ki pripada presečišču $`i`$ in enega, 
ki pripada presečišču $`i + 1`$) med katerima preteče najmanj časa (kar pomeni, da gre hypotrochoida direktno iz točke $`i`$ v točko $`i + 1`$).
Vrne seznam oblike `[[t_p1_out, t_p2_in], [t_p2_out, t_p3_in], ..., [t_p7_out, t_p1_in]]`.
Oznaka `in` pomeni, da krivulja v smeri urinega kazalca pripotuje v presečišče $`p_i`$, oznaka `out` pa, da iz njega v smeri urinega kazalca potuje naprej.

Odločila sem se, da bom hypotrochoido integrirala v smeri osi $`x`$, za kar sem poleg najdenih časov v presečiščih potrebovala še čas, 
ko krivulja doseže levi ektrem (čas ob katerem štarta od osi $`x`$ proti presečišču p1 oziroma čas, ko iz presečišča p7 pripotuje do $`x`$ osi). 
Ta čas sem izračunala z metodo gradientnega spusta. Dobra začetna približka za gradientni spust sta `t_p1_in` ali `t_p7_out`. Krivulja levi ektrem 
doseže ob času, ki sem ga označila `t_p0` in znaša 69.1150385071. Ob tem času krivulja zaključi prvi obhod.
<!--Ko sem gledala animacijo 
risanja krivulje, sem videla, da krivulja od časa
$`t = 0`$ začne (ob naraščanju `t`) potovati navzdol v presečišče p7, na začetek pa se vrne čez presečišče p1. Dobra začetna približka za gradientni spust sta zato 
`t_p1_in` ali `t_p7_out`.--> 

Krivuljo sem nato integrirala v smeri osi $`x`$ z uporabo formule (vir: [parametrični integrali](https://studentski.net/gradivo/ulj_fmf_fm1_ma1_sno_uporaba_integrala_01) strani 8 in 9)
```math
    I = \int ydx.
```
Ploščino sem dobila kot vsoto ploščine zgornjega dela hypotrochoide
```math
    I_1 = \int_{t\_p0}^{t\_p1\_in} ydx + \int_{t\_p1\_out}^{t\_p2\_in} ydx + \int_{t\_p2\_out}^{t\_p3\_in} ydx + \int_{t\_p3\_out}^{t\_p4\_in} ydx
```
in ploščine spodnjega dela hypotrochoide
```math
    I_2 = - \Big(\int_{t\_p0}^{t\_p7\_out} ydx + \int_{t\_p7\_in}^{t\_p6\_out} ydx + \int_{t\_p6\_in}^{t\_p5\_out} ydx + \int_{t\_p5\_in}^{t\_p4\_out} ydx \Big),
```
kjer vrednost integrala $`I_2`$ upoštevam z negativnim predznakom, saj ta del krivulje leži pod $`y`$ osjo (kar prinese minus), 
integriram pa v pravi smeri $`x`$ osi (kar prinese plus).

Zaradi simetrije hypotrochoide pa velja kar
```math
    I = I_1 + I_2 = 2 \cdot I_1.
```
Ploščina hypotrochoide znaša 14.1581975210. Za izračun integralov sem uporabila optimizirano Rombergovo metodo, ki namesto celotne tabele,
hrani le trenutno in prejšnjo vrstico tabele. Ploščino sem izračunala z uporabo funkcije `area_hypotrochoida(a, b, epsilon, animated = False)`.
Ob vrednosti parametra `animated = True` se ob računanju prikaže tudi animacija, ki prikazuje vrstni red integriranja. Z animacijo sem preverila, če 
res računam ploščine pravih odsekov krivulje.






