# Porazdelitvena funkcija normalne slučajne spremenljivke
V nalogi z uporabo različnih metod numeričnega integriranja računam vrednost funkcije porazdelitve verjetnosti, <!--porazdelitvene funkcije za standardno normalno porazdeljeno slučajno spremenljivko 
$`X \sim N(0,1)`$--> ki je podana s predpisom
```math
	\phi(x) = P(X \leq x) = \frac{1}{\sqrt{2\pi}}\int_{-\infty}^{x}e^{-\frac{t^2}{2\pi}}dt.
```
Vse metode so implementirane v skrpiti `porazdelitvena.jl`, grafi pa so izrisani z uporabo metod iz skripte `plots.jl`.</br>

Vrednost integrala se z večanjem $`x`$ od $`-\infty`$ do $`\infty`$ spreminja od 0 do $`\sqrt{\pi}`$. 
Opazila sem, da pri $`x \leq -15`$ funkcija $`\phi(x)`$ že doseže mejno vrednost 0, pri $`x \geq 15`$ pa mejno vrednost $`\sqrt{\pi}`$. Zato v vseh metodah v primerih, ko je $`x`$ 
izven intervala (-15, 15), brez računanja vrnem ustrezno vrednost.

Za funkcijo porazdelitvene verjetnosti poznamo eksaktno vrednost
(vir: [wolfram alpha](https://www.wolframalpha.com/input/?i=integrate+e%5E-(t%5E2%2F(2pi))%2F(sqrt(2pi))dt+from+-inf+to+x))
```math
    \phi(x) = \frac{1}{\sqrt{2\pi}}\int_{-\infty}^{x}e^{-\frac{t^2}{2\pi}}dt = \frac{\sqrt{\pi}}{2}(erf(\frac{x}{\sqrt{2\pi}})+1).
```
To vrednost v mojem programu izračuna funkcija `cumulative_distribution_function_exact(x)`.
```julia
cumulative_distribution_function_exact(-15) == 0
true
ecumulative_distribution_function_exact(-14) == 0
false
cumulative_distribution_function_exact(14) == sqrt(pi)
false
cumulative_distribution_function_exact(15)== sqrt(pi)
true
```

## Simpsonova 1/3 in Rombergova metoda
Najprej sem implementirala Simposnovo 1/3 in Rombergovo metodo. 
Da sem ju lahko uporabila za izračun danega integrala, sem se morala najprej znebiti spodnje meje $`-\infty`$. To sem storila z upoštevanjem 
simetrije funkcije porazdelitvene verjetnosti ter zveze
(vir: [wolfram alpha](https://www.wolframalpha.com/input/?i=integrate+e%5E-(t%5E2%2F(2pi))%2F(sqrt(2pi))dt+from+-inf+to+0))
 ```math
    \phi(0) = \frac{1}{\sqrt{2\pi}}\int_{-\infty}^{0}e^{-\frac{t^2}{2\pi}}dt = \sqrt{\pi}/2.
```
s čimer se računanje integrala v mejah ($`-\infty`$, $`x`$] prevede v računanje integrala v mejah [0, $`x`$], kateremu se prišteje vrednost $`\sqrt{\pi}/2`$
```math
    \phi(x) = \frac{1}{\sqrt{2\pi}}\int_{-\infty}^{x}e^{-\frac{t^2}{2\pi}}dt = \sqrt{\pi}/2 + \frac{1}{\sqrt{2\pi}}\int_{0}^{x}e^{-\frac{t^2}{2\pi}}dt.
```
### Simpsonova 1/3 metoda
Pri Simpsonovi metodi sem do rezultata, ki je natančen na vsaj 10 decimalnih mest, prišla tako, da sem število intervalov na območju [0, $`x`$] povečevala za faktor dva,
dokler sta se trenutni rezultat in tisti izračunan na dvakrat več intervalih po absolutni vrednosti razlikovala za 10<sup>-10</sup> ali več.
Ko je razlika padla na vsaj 11. decimalno mesto, sem za dovolj natančen rezultat vzela tistega izračunenega na več intervalih.
Spodnji graf prikazuje na koliko delov je bilo potrebno razdeliti integracijsko območje za dovolj natančen izračun integrala pri različnih $`x`$.<br/>
<!-- ![stevilo_intervalov](images_porazdelitvena/stevilo_intervalov.png "title-1")-->
<img src="images_porazdelitvena/stevilo_intervalov.png" alt="stevilo_intervalov" width="550" height="370"/>

Za izračun integrala za izbran $`x`$ in z želeno natančnostjo $`epsilon`$ po simpsonovi 1/3 metodi in 
izračun zadostnega števila intervalov se uporabi funkcijo `cumulative_distribution_function_numerical(x, epsilon; method = "simpson13")`.

### Rombergova metoda
Pri Rombergovi metodi sem na vsakem koraku računala absolutno razliko med zadnjim številom v trenutni vrstici Rombergove tabele 
ter zadnjim številom v prejšnji vrstici tabele, dokler razlika ni padla pod 10<sup>-10</sup>. Za dovolj natančen rezultat sem vzela tistega iz kasnješe vrstice.
Metodo sem optimizirala tako, da namesto celotne tabele hrani le trenutno in prejšnjo vrstico tabele.
Spodnji graf prikazuje velikost Rombergove tabele, ki pri danih $`x`$, zadošča za dovolj natančen izračun integrala. 
<br/>
<!-- ![velikost_tabele](images_porazdelitvena/velikost_tabele.png "title-2")-->
<img src="images_porazdelitvena/velikost_tabele.png" alt="velikost_tabele" width="550" height="370"/>

Za izračun integrala za izbran $`x`$ in z želeno natančnostjo $`epsilon`$ po Rombergovi metodi in 
izračun zadostne velikosti tabele se uporabi funkcijo `cumulative_distribution_function_numerical(x, epsilon; method = "romberg")`.

Grafa sta po pričakovanjih simetrična glede na $`y`$ os. Pri Rombergovi metodi se vidi, da širše kot je območje računanja,
večja tabela je potrebna za dovolj natančen izračun. Pri Simpsonovi metodi se z večanjem območja število intervalov naprej povečuje, kar je smiselno, 
vendar je za nekatere $`x`$ funkcija na intervalu očitno take oblike, da se jo da s polinomi druge stopnje lepo aproksimirati tudi na manj intervalih.
Grafa sta izrisana s funkcijo `plot_num_of_intervals_and_table_size(x_start, x_end, epsilon)`.




## Gauss-Legendrove in Gauss-Laguerrove kvadrature
Pri teh dveh metodah območja nisem delila na več delov, vendar sem metode implementirala tako, da pri danem $`x`$ poiščejo najnižjo 
še ustrezno stopnjo aproksimacijskega polinoma, da je integral izračunan dovolj natančno.
Ker v programskem jeziku Julia nisem našla oziroma mi ni uspelo namestiti funkcije za izračun ničel in uteži Gauss-Legendrovih in Gauss-Laguerrovih polinomov,
sem napisala python skripto, ki za podano število vozlišč oziroma stopnjo polinoma, izračuna ničle in uteži ter
jih zapiše v .txt dokument. Python skripta se požene znotraj funkcij implementiranih v jeziku Julia od koder potem ničle in uteži tudi preberem in jih uporabim za izračune.

### Gauss-Legendrove kvadrature
Ker v primeru te naloge poznam eksaktno vrednost integrala, ki ga računam, lahko najnižjo stopnjo aproksimacijskega polinoma za dovolj natančen izračun integrala dobim tako,
da funkcijo pod integralom na vsakem koraku aproksimiram s polinomom čedalje višje stopnje (pri čemer grem seveda le po lihih stopnjah) ter izračunam absolutno razliko 
med dobljeno ter eksaktno vrednostjo. Ko razlika postane manjša od 10<sup>-10</sup>, neham. Ker dostikrat eksaktne vrednosti integralov
ne poznamo, lahko vrednosti dobljene po Gauss-Lagendrovih kvadraturah primerjam recimo z vrednostjo dobljeno s Simpsonovo 1/3 ali Rombergovo metodo, ki sem ju že
implementirala.

Tako kot prej sem integral razbila na dva dela in ga računala le v mejah [0, $`x`$]. Funkcija 
`cumulative_distribution_function_numerical(x, epsilon; integral_value = I_romberg, method = "gauss")` 
izračuna vrednost integrala ter najmanjšo stopnjo polinoma s katerim lahko aproksimiram dano funkcijo.
V argument $`integral\_value`$ sem v zgornjem primeru vstavila vrednost integrala izračunanega z Rombergovo metodo.

### Gauss-Leguerrove kvadrature
Poleg zgoraj opisanih metod, ki smo jih obravnavali na predavanjih, sem implementirala tudi metodo, s katero lahko direktno izračunam integrale, 
ki imajo v eni od mej število $`\infty`$.

Metoda omogoča direkten izračun integrala oblike
```math
	\int_{0}^{\infty}e^{-u}f(u)du \approx \sum_{i=1}^{n}w_if(u_i),
```
ki ga aproksimiram z ustrezno vsoto produktov, kjer so $`u_i`$ ničle, ter $`w_i`$ uteži Gauss-Laguerrovih ortogonalnih polinomov.<!--<br/>-->
Zaradi simetrije funkcije $`f(t) = \frac{1}{\sqrt{2\pi}}e^{-\frac{t^2}{2\pi}}`$ velja enakost
```math
	\phi(x) = \int_{-\infty}^{x}f(t)du = \int_{-x}^{\infty}f(t)dt.
```
Da sem lahko uporabila Gauss-Laguerrove kvadrature, sem morala v spodnji meji namesto $`-x`$ dobiti število $`0`$, kar sem dosegla z uvedbo nove spremenljivke $`u = t + x`$. 
Velja $`t = u - x`$.  

S tem se zgornji integral preoblikuje v
```math
	\phi(x) = \int_{-x}^{\infty}f(t)dt = \int_{0}^{\infty}f(u - x)du.
```
Funkcijo pod integralom sem nato le še množila in delila s faktorjem $`e^{u - x}`$
```math
	\phi(x) = \int_{0}^{\infty}f(u - x)dt = \int_{0}^{\infty}f(u - x)e^{(u  - x)}e^{-(u - x)}dt = \int_{0}^{\infty}g(u - x)e^{-(u - x)}dt,
```
kjer sem uvedla novo funkcijo $`g(u - x) = f(u - x)e^{(u - x)}`$. V primeru te naloge velja $`g(u - x) = \frac{1}{\sqrt{2\pi}}e^{-\frac{(u - x)^2}{2\pi}}e^{(u - x)}`$. 
Enačbo sem nato lahko preoblikovala v tako, ki ustreza formuli za Gauss-Laguerrove kvadrature
```math
	\phi(x) = \int_{0}^{\infty}g(u - x)e^{-(u - x)}dt = e^{x}\int_{0}^{\infty}g(u - x)e^{-u}dt \approx e^{x}\sum_{i=1}^{n}w_ig(u_i - x).
```
Tako sem dobila spodnjo zvezo
```math
    \phi(x) = \frac{1}{\sqrt{2\pi}}e^{x}\int_{0}^{\infty}e^{-\frac{(u-x)^2}{2\pi}}e^{(u-x)}e^{-u}du \approx \frac{1}{\sqrt{2\pi}}e^{x}\sum_{i=1}^{n}w_ie^{-\frac{(u_i-x)^2}{2\pi}}e^{(u_i-x)}
```
<!--
```math
	\phi(x < 0) = e^{-x}\int_{0}^{\infty}e^{-\frac{(t+x)^2}{2\pi}}e^{(t+x)}e^{-t}dt \approx e^{-x}\sum_{i=1}^{n}w_ie^{-\frac{(t_i+x)^2}{2\pi}}e^{(t_i+x)}
```-->
Za izračun integrala in najnižje stopnje aproksimacijskega polinama po tej metodi se uporabi 
`cumulative_distribution_function_numerical(x, epsilon; integral_value = I_romberg, method = "gauss laguerre")`.


Integral kumulativne porazdelitvene funkcije se računa v mejah ($`-\infty`$, $`x`$]. Kadar je $`x`$ pozitivno število se torej računa ploščina pod večjim delom krivulje,
za kar potrebujem, 
če želim integral izračunati dovolj natančno, višjo stopnjo polinoma, kot kadar je $`x`$ negativen. Zato sem tudi tu upoštevala simetrijo porazdelitvene funkcije ter zvezo
(vir: [wolfram alpha](https://www.wolframalpha.com/input/?i=integrate+e%5E-(t%5E2%2F(2pi))%2F(sqrt(2pi))dt+from+-inf+to+inf))
```math
    \phi(\infty) = \frac{1}{\sqrt{2\pi}}\int_{-\infty}^{\infty}e^{-\frac{t^2}{2\pi}}dt = \sqrt{\pi}
```
in tako integral vedno izračunala za negativni $`x`$
```math
	\phi(x < 0) = \frac{1}{\sqrt{2\pi}}e^{-x}\int_{0}^{\infty}e^{-\frac{(t+x)^2}{2\pi}}e^{(t+x)}e^{-t}dt \approx \frac{1}{\sqrt{2\pi}}e^{-x}\sum_{i=1}^{n}w_ie^{-\frac{(t_i+x)^2}{2\pi}}e^{(t_i+x)}
```
ter rezultate za pozitivne $`x`$ dobila kot
```math
    \phi(x > 0) = \sqrt{\pi} - \phi(x < 0).
```
Za izračun integrala in najnižje stopnje aproksimacijskega polinama po tej metodi se uporabi 
`cumulative_distribution_function_numerical(x, epsilon; integral_value = I_romberg, method = "gauss laguerre better")`.


Najnižjo primerno stopnjo za dovolj natančen izračun integrala sem dobila na enak način kot pri metodi Gauss-Legendrovih kvadratur, 
le da sem tu upoštevala tudi sode stopnje polinomov.
Spodnji graf prikazuje primerjavo med najnižjo stopnjo polinoma pri Gauss-Legendrovi metodi, ter pri obeh Gauss-Laguerrovih metodah za različne $`x`$.
<!-- ![stopnje](images_porazdelitvena/stopnje.png "title-3")-->
<img src="images_porazdelitvena/stopnje.png" alt="stopnje" width="550" height="370"/>

Graf je izrisan s funkcijo `plot_order_of_polynomial(x_start, x_end, epsilon)`.

Ker pri Gauss-Legendrovih kvadraturah integral računam le na območju [0, $`x`$], je krivulja simetrična glede na $`y`$ os, 
stopnja aproksimacijskega polinoma pa se z oddaljenostjo od 0 veča, saj se računa ploščina pod čedalje večjim intervalom.
Pri Gauss-Laguerrovih kvadraturah integral računam na območju ($`-\infty`$, $`x`$], zato se stopnja aproksimacijskega polinoma z večanjem $`x`$ povečuje,
vidno pa je, da metoda, kjer integral vedno računam le za negativne $`x`$, pri $`x = 0`$ uporabi aproksimacijski polinom najvišje stopnje, nato pa se stopnje zopet
znižujejo, simetrično glede na $`y`$ os, kar je za pričakovati.

Metoda Gaussovih in metodi Gauss-Laguerrovih kvadratur so se izkazale za dosti bolj počasne od Simpsonove 1/3 in Rombergove metode, kar prikazujeta spodnja grafa 
povprečnih časov izvajanja metod. Za pozitivne $`x`$ je najbolj počasna navadna Gauss-Laguerrova metoda, ki z večanjem $`x`$ 
uporablja čedalje višje stopnje aproksimacijskih polinomov, saj s tem porabi dosti časa, 
da ustrezno stopnjo sploh najde. Medtem ko Simpsonova 1/3 in Rombergova metoda na celotnem intervalu računanja zelo hitro prideta do dovolj natančnega rezultata.
<!-- ![casi](casi.png)![casi2](casi_simpson_romberg.png "title-5")-->
<img src="images_porazdelitvena/casi_polinomi.png" alt="casi" width="550" height="370"/>
<img src="images_porazdelitvena/casi_simpson_romberg.png" alt="casi2" width="550" height="370"/>

Grafa sta izrisana s funkcijo `plot_time(x_start, x_end, epsilon)`.
