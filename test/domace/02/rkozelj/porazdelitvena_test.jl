# naj asistent ignorira spodnji dve vrstici
#cd("D:\\git\\Num-mat\\DN\\dn2\\nm-dn2\\test\\domace\\02\\rkozelj")
#include("porazdelitvena_test.jl")

include("../../../../src/domaca02/rkozelj/porazdelitvena.jl")

using Test

@testset "porazdelitvena" begin
    epsilon = 10^(-10)
    @testset "simpson13" begin
        println("simpson13")
        xs = LinRange(-15, 15, 31)
        for x in xs
            println("x ", x)
            aprox_value, _ = cumulative_distribution_function_numerical(x, epsilon; method = "simpson13")
            exact_value = cumulative_distribution_function_exact(x)

            #@test abs(abs(aprox_value - exact_value)/exact_value) < epsilon # relativna natančnost 10^(-10)
            @test abs(aprox_value - exact_value) < epsilon # natančnost na 10 decimalk
        end
    end

    @testset "romberg" begin
        println("romberg")
        xs = LinRange(-15, 15, 31)
        for x in xs
            println("x ", x)
            aprox_value, _ = cumulative_distribution_function_numerical(x, epsilon; method = "romberg")
            exact_value = cumulative_distribution_function_exact(x)

            #@test abs(abs(aprox_value - exact_value)/exact_value) < epsilon # relativna natančnost 10^(-10)
            @test abs(aprox_value - exact_value) < epsilon # natančnost na 10 decimalk
        end
    end

    @testset "gauss legendre" begin
        println("gauss legendre")
        xs = LinRange(-15, 15, 31)
        for x in xs
            println("x ", x)
            I_romberg, _ = cumulative_distribution_function_numerical(x, epsilon; method = "romberg")
            aprox_value, _ = cumulative_distribution_function_numerical(x, epsilon;  integral_value = I_romberg, method = "gauss")
            exact_value = cumulative_distribution_function_exact(x)

            #@test abs(abs(aprox_value - exact_value)/exact_value) < epsilon # relativna natančnost 10^(-10)
            @test abs(aprox_value - exact_value) < epsilon # natančnost na 10 decimalk
        end
    end

    @testset "gauss laguerre" begin
        println("gauss laguerre")
        xs = LinRange(-15, 15, 31)
        for x in xs
            println("x ", x)
            I_romberg, _ = cumulative_distribution_function_numerical(x, epsilon; method = "romberg")
            aprox_value, _ = cumulative_distribution_function_numerical(x, epsilon;  integral_value = I_romberg, method = "gauss laguerre")
            exact_value = cumulative_distribution_function_exact(x)

            #@test abs(abs(aprox_value - exact_value)/exact_value) < epsilon # relativna natančnost 10^(-10)
            @test abs(aprox_value - exact_value) < epsilon # natančnost na 10 decimalk
        end
    end

    @testset "gauss laguerre better" begin
        println("gauss laguerre better")
        xs = LinRange(-15, 15, 31)
        for x in xs
            println("x ", x)
            I_romberg, _ = cumulative_distribution_function_numerical(x, epsilon; method = "romberg")
            aprox_value, _ = cumulative_distribution_function_numerical(x, epsilon; integral_value = I_romberg, method = "gauss laguerre better")
            exact_value = cumulative_distribution_function_exact(x)

            #@test abs(abs(aprox_value - exact_value)/exact_value) < epsilon # relativna natančnost 10^(-10)
            @test abs(aprox_value - exact_value) < epsilon # natančnost na 10 decimalk
        end
    end
end