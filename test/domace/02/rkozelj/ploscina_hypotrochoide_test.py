import sys
import unittest
import numpy as np

sys.path.append("../../../../src/domaca02/rkozelj")

import ploscina_hypotrochoide as ph

class TestArea(unittest.TestCase):

    def test_area(self):
        """
        Preveri ali je izračunana ploščina hypotrochoide v pravih mejah.
        """
        a = 1
        b = -11/7
        epsilon = 10**(-10)
        area = ph.area_hypotrochoida(a, b, epsilon, animated = False)
        #preverim, če je ploščina manjša od (vsaj na oko) maximalne: na grafu v smeri x, -x, y in -y za 2 => 4*4 = 16
        self.assertTrue(area < 16)
        #preverim, če je ploščina večja od (vsaj na oko) manjše: na grafu v smeri x, -x, y in -y za 1.5 => 3*3 = 9
        self.assertTrue(area > 9)

    def test_intersection(self):
        """
        Preveri pravilnost newtonove metode za reševanje sistemov. 
        """
        f1 = lambda x, y, z: 5*x - y + 2*z - 12
        f2 = lambda x, y, z: 3*x + 8*y -2*z + 25
        f3 = lambda x, y, z: x + y + 4*z - 6
        
        F = np.array([f1, f2, f3])
        
        df1x = lambda x, y, z: 5
        df1y = lambda x, y, z: -1
        df1z = lambda x, y, z: 2
        
        df2x = lambda x, y, z: 3
        df2y = lambda x, y, z: 8
        df2z = lambda x, y, z: -2
        
        df3x = lambda x, y, z: 1
        df3y = lambda x, y, z: 1
        df3z = lambda x, y, z: 4
        
        
        J = np.array([[df1x, df1y, df1z],
                      [df2x, df2y, df2z],
                      [df3x, df3y, df3z]])
        
    
        x0 = np.array([0, 0, 0])
        epsilon = 10**(-10)
        
        #odvodi analitično
        x =ph.newton_method_sistem(F, x0, epsilon, J, max_num_of_it = 50)
        #print(x)
        #numerično izračunana rešitev je enaka analitično izračunani
        self.assertTrue((x == np.array([1., -3., 2.])).all())
        Fx = np.array([fun(*x) for fun in F])
        #ko v funkcije iz vektorja F vstavim dobljeno rešitev mora biti vrednost vsake funkcije enaka 0 
        self.assertTrue((Fx == np.array([0., 0., 0.])).all())
        
        
    def test_integration(self):
        """
        Preveri pravilnost rombergove metode.
        """
        epsilon = 10**(-10)
        
        g = lambda x: -5 + 4*x - 3*x**2 + 2*x**3 + x**4
        a, b = 10, -10
        Ig, n = ph.romberg(g, a, b, epsilon)
        Ig_tocna = -37900.0 # vir: https://www.wolframalpha.com/input/?i=intergate+-5+%2B+4*x+-+3*x**2+%2B+2*x**3+%2B+x**4+from+10+to+-10
        self.assertEqual(Ig, Ig_tocna)
        
        f = lambda x: np.sin(2*x) - x**2
        a, b = 0, 4
        If, n = ph.romberg(f, a, b, epsilon)
        If_tocna = -20.7605833164 # vir: https://www.wolframalpha.com/input/?i=intergate+sin(2*x)+-+x**2+from+0+to+4
        self.assertEqual(round(If, 10), If_tocna) #zaokrožim na 10 mest, ker računam z natančnostjo 10**(-10)
        
if __name__ == "__main__":
    unittest.main()
