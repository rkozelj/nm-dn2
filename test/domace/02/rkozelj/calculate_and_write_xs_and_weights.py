import numpy as np
import sys

def write_coefficients(file_name, n_legendre, n_laguerre):
    """
    Izračuna ničle in uteži Gauss-Legendrovih polinomov stopnje n_legendre in 
    ničle in uteži Gauss-Laguerrovih polinomov stopnje n_laguerre 
    ter jih zapiše v datoteko z imenom file_name.
    """
    xs_legendre, weights_legendre = np.polynomial.legendre.leggauss(n_legendre)
    xs_laguerre, weights_laguerre = np.polynomial.laguerre.laggauss(n_laguerre)
    
    xs_legendre = [str(round(x, 16)) for x in xs_legendre]
    weights_legendre = [str(round(x, 16)) for x in weights_legendre]

    xs_laguerre = [str(round(x, 16)) for x in xs_laguerre]
    weights_laguerre = [str(round(x, 16)) for x in weights_laguerre]
    
    with open(file_name, "w") as f:
        f.write("xs_legendre = [" + ", ".join(xs_legendre) + "]\n")
        f.write("weights_legendre = [" + ", ".join(weights_legendre) + "]\n")
        #f.write("\n")
        f.write("xs_laguerre = [" + ", ".join(xs_laguerre) + "]\n")
        f.write("weights_laguerre = [" + ", ".join(weights_laguerre) + "]\n")


def write_legendre_coefficients(file_name, n_legendre):
    """
    Izračuna ničle in uteži Gauss-Legendrovih polinomov stopnje n_legendre ter jih zapiše v datoteko z imenom file_name.
    """
    xs_legendre, weights_legendre = np.polynomial.legendre.leggauss(n_legendre)
    
    xs_legendre = [str(round(x, 16)) for x in xs_legendre]
    weights_legendre = [str(round(x, 16)) for x in weights_legendre]
    
    with open(file_name, "w") as f:
        f.write("xs_legendre = [" + ", ".join(xs_legendre) + "]\n")
        f.write("weights_legendre = [" + ", ".join(weights_legendre) + "]\n")
        #f.write("\n")
        f.write("xs_laguerre = []\n")
        f.write("weights_laguerre = []\n")


def write_laguerre_coefficients(file_name, n_laguerre):
    """
    Izračuna ničle in uteži Gauss-Laguerrovih polinomov stopnje n_laguerre ter jih zapiše v datoteko z imenom file_name.
    """
    xs_laguerre, weights_laguerre = np.polynomial.laguerre.laggauss(n_laguerre)
    
    xs_laguerre = [str(round(x, 16)) for x in xs_laguerre]
    weights_laguerre = [str(round(x, 16)) for x in weights_laguerre]
    
    with open(file_name, "w") as f:
        f.write("xs_legendre = []\n")
        f.write("weights_legendre = []\n")
        #f.write("\n")
        f.write("xs_laguerre = [" + ", ".join(xs_laguerre) + "]\n")
        f.write("weights_laguerre = [" + ", ".join(weights_laguerre) + "]\n")
        
    
if __name__ == "__main__":
    #n_legendre, n_laguerre = 2, 3
    n_legendre, n_laguerre = sys.argv[1], sys.argv[2]
    #print(n_legendre, n_laguerre)
    
    file_name = "xs_and_weights.txt"
    
    if n_legendre == "no value":
        write_laguerre_coefficients(file_name, int(n_laguerre))
    elif n_laguerre == "no value":
        write_legendre_coefficients(file_name, int(n_legendre))
    else:
        write_coefficients("xs_and_weights.txt", int(n_legendre), int(n_laguerre))
